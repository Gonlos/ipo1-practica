BEGIN TRANSACTION;
CREATE TABLE "union_Comp_Photos" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`idCompetition`	int(11),
	`idPhoto`	int(11)
);
CREATE TABLE "union_Comp_Gallery" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`idCompetition`	int(11),
	`idGallery`	int(11)
);
CREATE TABLE "union_Comp_Comp" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`idCompetition`	int(11),
	`idCompetitor`	int(11)
);
CREATE TABLE "union_Comp_CE" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`idCompetition`	int(11),
	`idCollaboratingEntity`	int(11)
);
CREATE TABLE "sponsors" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar(45),
	`photo`	longblob
);
CREATE TABLE "photos" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`value`	mediumblob
);
CREATE TABLE "organizingEntities" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar(45),
	`photo`	longblob
);
CREATE TABLE "competitors" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar(45),
	`surname`	varchar(45),
	`dni`	varchar(45),
	`email`	varchar(45),
	`bornDate`	datetime,
	`city`	varchar(45),
	`telephone`	varchar(45),
	`shirtSize`	varchar(45),
	`typeRunner`	varchar(45),
	`photo`	longblob,
	`sponsor`	int(11),
	`club`	int(11)
);
CREATE TABLE "competitions" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar(45),
	`description`	varchar(450),
	`date`	datetime,
	`limitInscriptionDate`	datetime,
	`city`	varchar(45),
	`level`	varchar(45),
	`distance`	int(11),
	`awards`	varchar(450),
	`fee`	double,
	`competitorsLimit`	int(11),
	`webLinks`	varchar(450),
	`complete`	bit(1),
	`modality`	varchar(45),
	`otherInfo`	varchar(450),
	`poster`	longblob,
	`organizingEntity`	int(11),
	`drawArea`	longblob
);
CREATE TABLE "collaboratingEntities" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar(45),
	`photo`	longblob
);
CREATE TABLE "clubs" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar(45),
	`photo`	longblob
);
COMMIT;
