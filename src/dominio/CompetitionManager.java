package dominio;

import java.sql.Blob;
import java.util.Date;
import java.util.Vector;

import persistencia.DBResource;
import persistencia.InterfaceAgent;
import persistencia.ResourceType;
import persistencia.SingletonAgent;
import presentacion.Editor.DrawArea.MyDrawArea;
import presentacion.Editor.DrawArea.MyDrawAreaContainer;

public class CompetitionManager {
    //Metodo para realizar una lectura en la base de datos
    static public Vector<Competition> readAllBasic() {
    	try {
			InterfaceAgent agent = SingletonAgent.getAgent();
			
			String sql = "SELECT id, name, poster FROM competitions;";
			
			Vector<DBResource> resouceList= new Vector<DBResource>();
			resouceList.add( new DBResource(ResourceType.INT) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.BLOB) );
			Vector<Vector<Object>> res = agent.read( sql, resouceList );
			
			Vector<Competition> competitions = new Vector<Competition>();
			
			for ( Vector<Object> data : res ) {				
				Competition competition = new Competition();
				competition.setId( (int) data.get(0) );
				competition.setName( (String) data.get(1) );
				competition.setPoster( new Image( (Blob) data.get(2) ) );
				
				competitions.add( competition );
			}
						
			return competitions;
    	} catch ( Exception e) {
    		int n = 11;
    	}
    	
    	return new Vector<Competition>();
    }
	
    //Metodo para realizar una lectura en la base de datos
    public boolean read(Competition competition) {
    	try {
			InterfaceAgent agent = SingletonAgent.getAgent();
			
			String sql = "SELECT name, description, date, limitInscriptionDate, city, level, distance, " +
						"awards, fee, competitorsLimit, webLinks, complete, modality, otherInfo, poster, " +
						"organizingEntity, drawArea FROM competitions WHERE id = " + competition.getId() + ";";
			
			Vector<DBResource> resouceList= new Vector<DBResource>();
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.DATE) );
			resouceList.add( new DBResource(ResourceType.DATE) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.INT) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.DOUBLE) );
			resouceList.add( new DBResource(ResourceType.INT) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.BOOLEAN) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.BLOB) );
			resouceList.add( new DBResource(ResourceType.INT) );
			resouceList.add( new DBResource(ResourceType.BLOB) );
	    	
			Vector<Vector<Object>> res = agent.read( sql, resouceList );
			
			Vector<Object> data = res.get(0);
			
			int i = 0;
			competition.setName( (String) data.get(i++) );
			competition.setDescription( (String) data.get(i++) );
			competition.setDate( (Date) data.get(i++) );
			competition.setLimitInscriptionDate( (Date) data.get(i++) );
			competition.setCity( (String) data.get(i++) );
			competition.setLevel( (String) data.get(i++) );
			competition.setDistance( (int) data.get(i++) );
			competition.setAward( (String) data.get(i++) );
			competition.setFee( (double) data.get(i++) );
			competition.setCompetitorsLimit( (int) data.get(i++) );
			competition.setWebLinks( (String) data.get(i++) );
			competition.setComplete( (boolean) data.get(i++) );
			competition.setModality( (String) data.get(i++) );
			competition.setOtherInfo( (String) data.get(i++) );
			
			competition.setPoster( new Image( (Blob) data.get(i++) ) );
			
			OrganizingEntity organizingEntity = new OrganizingEntity();
			organizingEntity.setId( (int) data.get(i++) );
			organizingEntity.read();
			competition.setOrganizingEntity( organizingEntity );
			
			competition.setGallery( PhotoManager.readAll( competition ) );
			competition.setCollaboratingEntities( CollaboratingEntityManager.readAll( competition ) );
			competition.setCompetitors( CompetitorManager.readAll( competition ) );
			
			MyDrawAreaContainer myDrawAreaContainer = new MyDrawAreaContainer();
			myDrawAreaContainer.setBlob( (Blob) data.get(i++) );
			competition.setMyDrawArea( myDrawAreaContainer.getMyDrawArea() );
			
			return true;
    	} catch ( Exception e) {
    		int n = 11;
    	}
    	
    	return false;
    }
    
    //Metodo para realizar una insercion en la base de datos
    public boolean insert(Competition competition) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();
	    	
			String sql = "INSERT INTO competitions( name, description, date, limitInscriptionDate, city, level, distance, " +
					"awards, fee, competitorsLimit, webLinks, complete, modality, otherInfo, poster, " +
					"organizingEntity, drawArea ) VALUES ( '" +
					competition.getName() + "', '" +
					competition.getDescription() + "', " +
					"?" + ", " +
					"?" + ", '" +
					competition.getCity() + "', '" +
					competition.getLevel() + "', " +
					competition.getDistance() + ", '" +
					competition.getAward() + "', " +
					competition.getFee() + ", " +
					competition.getCompetitorsLimit() + ", '" +
					competition.getWebLinks() + "', " +
					"?" + ", '" +
					competition.getModality() + "', '" +
					competition.getOtherInfo() + "', " +
					"?" + ", " +
					competition.getOrganizingEntity().getId() + ", " +
					"? );";
	    	
	    	Vector<DBResource> resouceList= new Vector<DBResource>();
	    	
	    	DBResource dbr;
	    	dbr = new DBResource(ResourceType.DATE);
	    	dbr.setValue( competition.getDate() );
	    	resouceList.add( dbr );
	    	
	    	dbr = new DBResource(ResourceType.DATE);
	    	dbr.setValue( competition.getLimitInscriptionDate() );
	    	resouceList.add( dbr );
	    
	    	dbr = new DBResource(ResourceType.BOOLEAN);
	    	dbr.setValue( competition.isComplete() );
	    	resouceList.add( dbr );
	    	
	    	dbr = new DBResource(ResourceType.BLOB);
	    	dbr.setValue( competition.getPoster() );
	    	resouceList.add( dbr );
	    	
	    	dbr = new DBResource(ResourceType.BLOB);
	    	dbr.setValue( new MyDrawAreaContainer( competition.getMyDrawArea() ) );
	    	resouceList.add( dbr );
	    	
	    	int id = agent.modifyWithID(sql, resouceList);
	    	competition.setId( id );
	    	
			competition.setGallery( PhotoManager.readAll( competition ) );
			competition.setCollaboratingEntities( CollaboratingEntityManager.readAll( competition ) );
			competition.setCompetitors( CompetitorManager.readAll( competition ) );
			
			CompetitorManager.saveAll(competition);
			
			CollaboratingEntityManager.saveAll(competition);
	    	
	    	return true;
    	} catch ( Exception e) {
    		int sd = 0;
    	}
    	
    	return false;
    }
    
    //Metodo para realizar una modificacion en la base de datos
    public boolean update(Competition competition) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();	    	
			
	    	String sql = "UPDATE competitions SET "
	    			+ "name = ?,"
	    			+ "description = ?,"
	    			+ "date = ?,"
	    			+ "limitInscriptionDate = ?,"
	    			+ "city = ?,"
	    			+ "level = ?,"
	    			+ "distance = ?,"
	    			+ "awards = ?,"
	    			+ "fee = ?,"
	    			+ "competitorsLimit = ?,"
	    			+ "webLinks = ?,"
	    			+ "complete = ?,"
	    			+ "modality = ?,"
	    			+ "otherInfo = ?,"
	    			+ "poster = ?,"
	    			+ "organizingEntity = ?, "
	    			+ "drawArea = ? "
	    			+ "WHERE id = " + competition.getId() + ";";
	
			Vector<DBResource> resouceList= new Vector<DBResource>();

			resouceList.add( new DBResource( ResourceType.STRING, competition.getName() ) );
			resouceList.add( new DBResource( ResourceType.STRING, competition.getDescription() ) );
			resouceList.add( new DBResource( ResourceType.DATE, competition.getDate() ) );
			resouceList.add( new DBResource( ResourceType.DATE, competition.getLimitInscriptionDate() ) );
			resouceList.add( new DBResource( ResourceType.STRING, competition.getCity() ) );
			resouceList.add( new DBResource( ResourceType.STRING, competition.getLevel() ) );
			resouceList.add( new DBResource( ResourceType.INT, competition.getDistance() ) );
			resouceList.add( new DBResource( ResourceType.STRING, competition.getAward() ) );
			resouceList.add( new DBResource( ResourceType.DOUBLE, competition.getFee() ) );
			resouceList.add( new DBResource( ResourceType.INT, competition.getCompetitorsLimit() ) );
			resouceList.add( new DBResource( ResourceType.STRING, competition.getWebLinks() ) );
			resouceList.add( new DBResource( ResourceType.BOOLEAN, competition.isComplete() ) );
			resouceList.add( new DBResource( ResourceType.STRING, competition.getModality() ) );
			resouceList.add( new DBResource( ResourceType.STRING, competition.getOtherInfo() ) );
			resouceList.add( new DBResource( ResourceType.BLOB, competition.getPoster() ) );
			resouceList.add( new DBResource( ResourceType.INT, competition.getOrganizingEntity().getId() ) );
			resouceList.add( new DBResource( ResourceType.BLOB, new MyDrawAreaContainer( competition.getMyDrawArea() ) ) );
	    	
	    	boolean noError;
	    	
	    	noError = 1 == agent.modify(sql, resouceList);
	    	if ( !noError ) return false;
	    	
	    	noError = CompetitorManager.delAll(competition);
	    	if ( !noError ) return false;
	    	
	    	noError = CompetitorManager.saveAll(competition);
	    	if ( !noError ) return false;
	    	
	    	noError = CollaboratingEntityManager.delAll(competition);
	    	if ( !noError ) return false;
	    	
	    	noError = CollaboratingEntityManager.saveAll(competition);
			
			return noError;
    	} catch ( Exception e) {
    	}
    	
    	return false;
    }
    
    //Metodo para realizar una eliminacion en la base de datos
    public boolean delete(Competition competition) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();
	    	String sql = "DELETE FROM competitions WHERE id =  " + competition.getId() + ";";
	    	
	    	boolean noError;
	    	
	    	noError = 1 == agent.modify(sql);
	    	if ( !noError ) return false;
	    	
	    	noError = CompetitorManager.delAll(competition);
	    	if ( !noError ) return false;
	    	
	    	noError = CollaboratingEntityManager.delAll(competition);
	    	
	    	return noError;
    	} catch ( Exception e) {
    	}
    	
    	return false;
    }
}
