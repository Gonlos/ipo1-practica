package dominio;

import java.sql.Blob;
import java.util.Date;
import java.util.Vector;

import persistencia.DBResource;
import persistencia.InterfaceAgent;
import persistencia.ResourceType;
import persistencia.SingletonAgent;

public class OrganizingEntityManager {
    //Metodo para realizar una lectura en la base de datos
    static public Vector<OrganizingEntity> readAllBasic() {
    	try {
			InterfaceAgent agent = SingletonAgent.getAgent();
			
			String sql = "SELECT id, name, photo FROM organizingEntities;";
			
			Vector<DBResource> resouceList= new Vector<DBResource>();
			resouceList.add( new DBResource(ResourceType.INT) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.BLOB) );
			Vector<Vector<Object>> res = agent.read( sql, resouceList );
			
			Vector<OrganizingEntity> organizingEntities = new Vector<OrganizingEntity>();
			
			for ( Vector<Object> data : res ) {				
				OrganizingEntity organizingEntity = new OrganizingEntity();
				organizingEntity.setId( (int) data.get(0) );
				organizingEntity.setName( (String) data.get(1) );
				organizingEntity.setPhoto( new Image( (Blob) data.get(2) ) );
				
				organizingEntities.add( organizingEntity );
			}
						
			return organizingEntities;
    	} catch ( Exception e) {
    		int n = 11;
    	}
    	
    	return new Vector<OrganizingEntity>();
    }
	
    //Metodo para realizar una lectura en la base de datos
    public boolean read(OrganizingEntity organizingEntity) {
    	return false;
    }
    
    //Metodo para realizar una insercion en la base de datos
    public boolean insert(OrganizingEntity organizingEntity) {
    	return false;
    }
    
    //Metodo para realizar una modificacion en la base de datos
    public boolean update(OrganizingEntity organizingEntity) {
    	return false;
    }
    
    //Metodo para realizar una eliminacion en la base de datos
    public boolean delete(OrganizingEntity organizingEntity) {
    	return false;
    }
}
