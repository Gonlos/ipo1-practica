package dominio;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.ImageIcon;

import persistencia.DBResource;
import persistencia.InterfaceAgent;
import persistencia.ResourceType;
import persistencia.SingletonAgent;

public class Photo {
	private PhotoManager manager;
	
	private int id;
	private Image value;
	
	public Photo() {
		this.manager = new PhotoManager();
		
		this.id = 0;
		this.value = null;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Image getValue() {
		return value;
	}
	
	public void setValue(Image value) {
		this.value = value;
	}
	
    //Metodo para realizar una lectura en la base de datos
    public boolean read() {
    	return this.manager.read( this );
    }
    
    //Metodo para realizar una insercion en la base de datos
    public boolean insert() {
    	return this.manager.insert( this );
    }
    
    //Metodo para realizar una modificacion en la base de datos
    public boolean update() {
    	return this.manager.update( this );
    }
    
    //Metodo para realizar una eliminacion en la base de datos
    public boolean delete() {
    	return this.manager.delete( this );
    }
}
