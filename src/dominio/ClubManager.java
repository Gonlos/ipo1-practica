package dominio;

import java.sql.Blob;
import java.util.Date;
import java.util.Vector;

import persistencia.DBResource;
import persistencia.InterfaceAgent;
import persistencia.ResourceType;
import persistencia.SingletonAgent;

public class ClubManager {
    //Metodo para realizar una lectura en la base de datos
    static public Vector<Club> readAllBasic() {
    	try {
			InterfaceAgent agent = SingletonAgent.getAgent();
			
			String sql = "SELECT id, name, photo FROM clubs;";
			
			Vector<DBResource> resouceList= new Vector<DBResource>();
			resouceList.add( new DBResource(ResourceType.INT) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.BLOB) );
			Vector<Vector<Object>> res = agent.read( sql, resouceList );
			
			Vector<Club> clubs = new Vector<Club>();
			
			for ( Vector<Object> data : res ) {				
				Club club = new Club();
				club.setId( (int) data.get(0) );
				club.setName( (String) data.get(1) );
				club.setPhoto( new Image( (Blob) data.get(2) ) );
				
				clubs.add( club );
			}
						
			return clubs;
    	} catch ( Exception e) {
    		int n = 11;
    	}
    	
    	return new Vector<Club>();
    }
	
    //Metodo para realizar una lectura en la base de datos
    public boolean read(Club club) {
    	try {
			InterfaceAgent agent = SingletonAgent.getAgent();
			
			String sql = "SELECT name, photo FROM clubs WHERE id = " + club.getId() + ";";
			
			Vector<DBResource> resouceList= new Vector<DBResource>();
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.BLOB) );
	    	
			Vector<Vector<Object>> res = agent.read( sql, resouceList );
			
			Vector<Object> data = res.get(0);
			
			club.setName( (String) data.get(0) );		
			club.setPhoto( new Image( (Blob) data.get(1) ) );
			
			return true;
    	} catch ( Exception e) {
    		int n = 11;
    	}
    	
    	return false;
    }
    
    //Metodo para realizar una insercion en la base de datos
    public boolean insert(Club club) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();
	    	String sql = "INSERT INTO clubs( name, photo ) VALUES ( " +
	    					club.getName() + ", " +
	    					"? , " + " );";
	    	
	    	Vector<DBResource> resouceList= new Vector<DBResource>();
	    	
	    	DBResource dbr;
	    
	    	dbr = new DBResource(ResourceType.BLOB);
	    	dbr.setValue( club.getPhoto() );
	    	resouceList.add( dbr );
	    	
	    	int id = agent.modifyWithID(sql, resouceList);
	    	club.setId( id );
	    	
	    	return true;
    	} catch ( Exception e) {
    		int sd = 0;
    	}
    	
    	return false;
    }
    
    //Metodo para realizar una modificacion en la base de datos
    public boolean update(Club club) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();
	    	
	    	
	    	String sql = "UPDATE clubs SET " +
	    			"name = " + club.getName() + ", " +
					"photo = ? " +
					"WHERE id = " + club.getId() + ";";
	
			Vector<DBResource> resouceList= new Vector<DBResource>();
			
			DBResource dbr;
			dbr = new DBResource(ResourceType.BLOB);
			dbr.setValue( club.getPhoto() );
			resouceList.add( dbr );
	    	
	    	return 1 == agent.modify(sql, resouceList);
    	} catch ( Exception e) {
    	}
    	
    	return false;
    }
    
    //Metodo para realizar una eliminacion en la base de datos
    public boolean delete(Club club) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();
	    	String sql = "DELETE FROM clubs WHERE id =  " + club.getId() + ";";
	    	
	    	return 1 == agent.modify( sql );
    	} catch ( Exception e) {
    	}
    	
    	return false;
    }
}
