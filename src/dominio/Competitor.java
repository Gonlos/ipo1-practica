package dominio;

import java.util.Date;
import java.util.Vector;

public class Competitor {
	private CompetitorManager manager;
	
	private int id;
	private String name;
	private String surname;
	private String dni;
	private String email;
	private Date bornDate;
	private String city;
	private String telephone;
	private String shirtSize;
	private String typeRunner;
	private Image photo;
	private Sponsor sponsor;
	private Club club;
	
	public Competitor() {
		this.manager = new CompetitorManager();
		
		this.id = 0;
		this.name = "";
		this.surname = "";
		this.dni = "";
		this.email = "";
		this.bornDate = new Date();
		this.city = "";
		this.telephone = "";
		this.shirtSize = "";
		this.typeRunner = "";
		this.photo = new Image();
		this.sponsor = new Sponsor();
		this.club = new Club();
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getDni() {
		return dni;
	}
	
	public void setDni(String dni) {
		this.dni = dni;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Date getBornDate() {
		return bornDate;
	}
	
	public void setBornDate(Date bornDate) {
		this.bornDate = bornDate;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getTelephone() {
		return telephone;
	}
	
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	public String getShirtSize() {
		return shirtSize;
	}
	
	public void setShirtSize(String shirtSize) {
		this.shirtSize = shirtSize;
	}
	
	public String getTypeRunner() {
		return typeRunner;
	}
	
	public void setTypeRunner(String typeRunner) {
		this.typeRunner = typeRunner;
	}
	
	public Image getPhoto() {
		return photo;
	}
	
	public void setPhoto(Image photo) {
		this.photo = photo;
	}
	
	public Sponsor getSponsor() {
		return sponsor;
	}
	
	public void setSponsor(Sponsor sponsor) {
		this.sponsor = sponsor;
	}
	
	public Club getClub() {
		return club;
	}
	
	public void setClub(Club club) {
		this.club = club;
	}
	
    public boolean read() {
    	return this.manager.read( this );
    }
    
    //Metodo para realizar una insercion en la base de datos
    public boolean insert() {
    	return this.manager.insert( this );
    }
    
    //Metodo para realizar una modificacion en la base de datos
    public boolean update() {
    	return this.manager.update( this );
    }
    
    //Metodo para realizar una eliminacion en la base de datos
    public boolean delete() {
    	return this.manager.delete( this );
    }
    
    @Override
    public String toString() {
    	return this.getName() + " " + this.getSurname();
    }
    
    @Override
    public boolean equals(Object obj) {
    	if ( obj == null ) return false;
    	if ( !obj.getClass().equals( this.getClass() )) return false;
    	return this.id == ((Competitor) obj).id;
    }
}