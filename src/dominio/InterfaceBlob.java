package dominio;

import java.sql.Blob;

public interface InterfaceBlob {
	public boolean getBlob( Blob blob );
	
	public boolean setBlob( Blob blob );
}
