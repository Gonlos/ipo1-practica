package dominio;

public class CollaboratingEntity {
	private CollaboratingEntityManager manager;
	
	private int id;
	private String name;
	private Image photo;
	
	public CollaboratingEntity() {
		this.manager = new CollaboratingEntityManager();
		
		this.id = 0;
		this.name = "";
		this.photo = new Image();
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Image getPhoto() {
		return photo;
	}
	
	public void setPhoto(Image photo) {
		this.photo = photo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
    //Metodo para realizar una lectura en la base de datos
    public boolean read() {
    	return this.manager.read( this );
    }
    
    //Metodo para realizar una insercion en la base de datos
    public boolean insert() {
    	return this.manager.insert( this );
    }
    
    //Metodo para realizar una modificacion en la base de datos
    public boolean update() {
    	return this.manager.update( this );
    }
    
    //Metodo para realizar una eliminacion en la base de datos
    public boolean delete() {
    	return this.manager.delete( this );
    }
    
    @Override
    public String toString() {
    	return this.getName();
    }
    
    @Override
    public boolean equals(Object obj) {
    	if ( obj == null ) return false;
    	if ( !obj.getClass().equals( this.getClass() )) return false;
    	return this.id == ((CollaboratingEntity) obj).id;
    }
}
