package dominio;

import java.util.Date;
import java.util.Vector;

import presentacion.Editor.DrawArea.MyDrawArea;

public class Competition {
	private CompetitionManager manager;
	
	private int id;
	private String name;
	private String description;
	private Date date;
	private Date limitInscriptionDate;
	private String city;
	private String level;
	private int distance;
	private String award;
	private double fee;
	private int competitorsLimit;
	private String webLinks;
	private boolean complete;
	private String modality;
	private String otherInfo;
	private Image poster;
	
	private OrganizingEntity organizingEntity;
	private Vector<Photo> gallery;
	
	private Vector<CollaboratingEntity> collaboratingEntities;
	private Vector<Competitor> competitors;
	
	private MyDrawArea myDrawArea;
	
	public Competition() {
		this.manager = new CompetitionManager();
		
		this.id = 0;
		this.name = "";
		this.description = "";
		this.date = new Date();
		this.limitInscriptionDate = new Date();
		this.city = "";
		this.level = "";
		this.distance = 0;
		this.award = "";
		this.fee = 0.0;
		this.competitorsLimit = 0;
		this.webLinks = "";
		this.complete = false;
		this.modality = "";
		this.otherInfo = "";
		this.poster = new Image();
		 
		this.organizingEntity = new OrganizingEntity();
		this.gallery = new Vector<Photo>();
			
		this.collaboratingEntities = new Vector<CollaboratingEntity>();
		this.competitors = new Vector<Competitor>();
		
		this.myDrawArea = new MyDrawArea();
	}

	public CompetitionManager getManager() {
		return manager;
	}

	public void setManager(CompetitionManager manager) {
		this.manager = manager;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getLimitInscriptionDate() {
		return limitInscriptionDate;
	}

	public void setLimitInscriptionDate(Date limitInscriptionDate) {
		this.limitInscriptionDate = limitInscriptionDate;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public String getAward() {
		return award;
	}

	public void setAward(String award) {
		this.award = award;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	public int getCompetitorsLimit() {
		return competitorsLimit;
	}

	public void setCompetitorsLimit(int competitorsLimit) {
		this.competitorsLimit = competitorsLimit;
	}

	public String getWebLinks() {
		return webLinks;
	}

	public void setWebLinks(String webLinks) {
		this.webLinks = webLinks;
	}

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	public String getModality() {
		return modality;
	}

	public void setModality(String modality) {
		this.modality = modality;
	}

	public String getOtherInfo() {
		return otherInfo;
	}

	public void setOtherInfo(String otherInfo) {
		this.otherInfo = otherInfo;
	}

	public Image getPoster() {
		return poster;
	}

	public void setPoster(Image poster) {
		this.poster = poster;
	}

	public OrganizingEntity getOrganizingEntity() {
		return organizingEntity;
	}

	public void setOrganizingEntity(OrganizingEntity organizingEntity) {
		this.organizingEntity = organizingEntity;
	}

	public Vector<Photo> getGallery() {
		return gallery;
	}

	public void setGallery(Vector<Photo> gallery) {
		this.gallery = gallery;
	}

	public Vector<CollaboratingEntity> getCollaboratingEntities() {
		return collaboratingEntities;
	}

	public void setCollaboratingEntities(
			Vector<CollaboratingEntity> collaboratingEntities) {
		this.collaboratingEntities = collaboratingEntities;
	}

	public Vector<Competitor> getCompetitors() {
		return competitors;
	}

	public void setCompetitors(Vector<Competitor> competitors) {
		this.competitors = competitors;
	}
	
	//Metodo para realizar una selección en la base de datos
    public boolean read() {
    	return this.manager.read( this );
    }
    
    //Metodo para realizar una insercion en la base de datos
    public boolean insert() {
    	return this.manager.insert( this );
    }
    
    //Metodo para realizar una modificacion en la base de datos
    public boolean update() {
    	return this.manager.update( this );
    }
    
    //Metodo para realizar una eliminacion en la base de datos
    public boolean delete() {
    	return this.manager.delete( this );
    }
    
    @Override
    public String toString() {
    	return this.getName();
    }
    
    @Override
    public boolean equals(Object obj) {
    	if ( obj == null ) return false;
    	if ( !obj.getClass().equals( this.getClass() )) return false;
    	return this.id == ((Competition) obj).id;
    }

	public MyDrawArea getMyDrawArea() {
		return myDrawArea;
	}

	public void setMyDrawArea(MyDrawArea myDrawArea) {
		this.myDrawArea = myDrawArea;
	}
}