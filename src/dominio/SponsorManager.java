package dominio;

import java.sql.Blob;
import java.util.Date;
import java.util.Vector;

import persistencia.DBResource;
import persistencia.InterfaceAgent;
import persistencia.ResourceType;
import persistencia.SingletonAgent;

public class SponsorManager {
    //Metodo para realizar una lectura en la base de datos
    static public Vector<Sponsor> readAllBasic() {
    	try {
			InterfaceAgent agent = SingletonAgent.getAgent();
			
			String sql = "SELECT id, name, photo FROM sponsors;";
			
			Vector<DBResource> resouceList= new Vector<DBResource>();
			resouceList.add( new DBResource(ResourceType.INT) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.BLOB) );
			Vector<Vector<Object>> res = agent.read( sql, resouceList );
			
			Vector<Sponsor> sponsors = new Vector<Sponsor>();
			
			for ( Vector<Object> data : res ) {				
				Sponsor sponsor = new Sponsor();
				sponsor.setId( (int) data.get(0) );
				sponsor.setName( (String) data.get(1) );
				sponsor.setPhoto( new Image( (Blob) data.get(2) ) );
				
				sponsors.add( sponsor );
			}
						
			return sponsors;
    	} catch ( Exception e) {
    		int n = 11;
    	}
    	
    	return new Vector<Sponsor>();
    }
	
    //Metodo para realizar una lectura en la base de datos
    public boolean read(Sponsor sponsor) {
    	try {
			InterfaceAgent agent = SingletonAgent.getAgent();
			
			String sql = "SELECT name, photo FROM sponsors WHERE id = " + sponsor.getId() + ";";
			
			Vector<DBResource> resouceList= new Vector<DBResource>();
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.BLOB) );
	    	
			Vector<Vector<Object>> res = agent.read( sql, resouceList );
			
			Vector<Object> data = res.get(0);
			
			sponsor.setName( (String) data.get(0) );		
			sponsor.setPhoto( new Image( (Blob) data.get(1) ) );
			
			return true;
    	} catch ( Exception e) {
    		int n = 11;
    	}
    	
    	return false;
    }
    
    //Metodo para realizar una insercion en la base de datos
    public boolean insert(Sponsor sponsor) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();
	    	String sql = "INSERT INTO sponsors( name, photo ) VALUES ( " +
	    					sponsor.getName() + ", " +
	    					"? , " + " );";
	    	
	    	Vector<DBResource> resouceList= new Vector<DBResource>();
	    	
	    	DBResource dbr;
	    
	    	dbr = new DBResource(ResourceType.BLOB);
	    	dbr.setValue( sponsor.getPhoto() );
	    	resouceList.add( dbr );
	    	
	    	int id = agent.modifyWithID(sql, resouceList);
	    	sponsor.setId( id );
	    	
	    	return true;
    	} catch ( Exception e) {
    		int sd = 0;
    	}
    	
    	return false;
    }
    
    //Metodo para realizar una modificacion en la base de datos
    public boolean update(Sponsor sponsor) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();
	    	
	    	
	    	String sql = "UPDATE sponsors SET " +
	    			"name = " + sponsor.getName() + ", " +
					"photo = ? " +
					"WHERE id = " + sponsor.getId() + ";";
	
			Vector<DBResource> resouceList= new Vector<DBResource>();
			
			DBResource dbr;
			dbr = new DBResource(ResourceType.BLOB);
			dbr.setValue( sponsor.getPhoto() );
			resouceList.add( dbr );
	    	
	    	return 1 == agent.modify(sql, resouceList);
    	} catch ( Exception e) {
    	}
    	
    	return false;
    }
    
    //Metodo para realizar una eliminacion en la base de datos
    public boolean delete(Sponsor sponsor) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();
	    	String sql = "DELETE FROM sponsors WHERE id =  " + sponsor.getId() + ";";
	    	
	    	return 1 == agent.modify( sql );
    	} catch ( Exception e) {
    	}
    	
    	return false;
    }
}
