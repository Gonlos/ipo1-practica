package dominio;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.ImageIcon;

public class Image implements InterfaceBlob {
	private ImageIcon image;
	//private String imageExt;
	
	public Image() {
		this.image = new ImageIcon( getClass().getClassLoader().getResource("dominio/Images/default_image.png") );
	}
	
	public Image( Blob blob ) {
		this.setBlob(blob);
	}
	
	public void loadImage( String path ) {
		this.image = new ImageIcon( path );
	}
	
	public ImageIcon getImageIcon() {
		return this.image;
	}

	public boolean setImage( byte[] imageData ) {
		try {
			this.image = new ImageIcon( imageData );
			
	    	return true;
		} catch ( Exception e ) {
		}
		
		return false;
	}
	
	public boolean getBlob( Blob blob ) {
		try {
		    ObjectOutputStream oos;
		    oos = new ObjectOutputStream( blob.setBinaryStream(1) );
		    oos.writeObject( this.image );
		    oos.close();
		    
	    	return true;
		} catch ( Exception e ) {
		}
		
		return false;
	}
	
	
	public boolean setBlob( Blob blob ) {
		try {
			 ObjectInputStream ois = null;
			 ois = new ObjectInputStream( blob.getBinaryStream());
			 this.image = (ImageIcon) ois.readObject(); 

			 return true;
		} catch ( Exception e ) {
		}
		
		return false;
	}
}
