package dominio;

import java.io.ByteArrayInputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Vector;

import persistencia.InterfaceAgent;
import persistencia.ResourceType;
import persistencia.SingletonAgent;
import persistencia.DBResource;

public class PhotoManager {
    //Metodo para realizar una lectura en la base de datos
    static public Vector<Photo> readAll( Competition competition ) {
    	try {
			InterfaceAgent agent = SingletonAgent.getAgent();
			
			String sql = "SELECT p.id, p.value FROM photos p, union_Comp_Photos u " + 
							"WHERE u.idCompetition = " + competition.getId() + " AND u.idPhoto = p.id;";
			
			Vector<DBResource> resouceList= new Vector<DBResource>();
			resouceList.add( new DBResource(ResourceType.INT) );
			resouceList.add( new DBResource(ResourceType.BLOB) );
			Vector<Vector<Object>> res = agent.read( sql, resouceList );
			
			Vector<Photo> photos = new Vector<Photo>();
			
			for ( Vector<Object> data : res ) {				
				Photo photo = new Photo();
				photo.setId( (int) data.get(0) );
				photo.setValue( new Image( (Blob) data.get(1) ) );
				
				photos.add( photo );
			}
						
			return photos;
    	} catch ( Exception e) {
    		int n = 11;
    	}
    	
    	return new Vector<Photo>();
    }
	
    //Metodo para realizar una lectura en la base de datos
    public boolean read(Photo photo) {
    	try {
			InterfaceAgent agent = SingletonAgent.getAgent();
			
			String sql = "SELECT value FROM photos WHERE id = " + photo.getId() + ";";
			
			Vector<DBResource> resouceList= new Vector<DBResource>();
	    	DBResource dbr = new DBResource(ResourceType.BLOB);
	    	resouceList.add( dbr );
	    	
			Vector<Vector<Object>> res = agent.read( sql, resouceList );
			Blob imageData = (Blob) res.get(0).get(0);
			
			Image img = new Image();
			img.setBlob(imageData);
			
			photo.setValue( img );
			
			return true;
    	} catch ( Exception e) {
    		int n = 11;
    	}
    	
    	return false;
    }
    
    //Metodo para realizar una insercion en la base de datos
    public boolean insert(Photo photo) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();
	    	String sql = "INSERT INTO photos( value ) VALUES ( ? );";
	    	
	    	Vector<DBResource> resouceList= new Vector<DBResource>();
	    	
	    	DBResource dbr = new DBResource(ResourceType.BLOB);
	    	dbr.setValue( photo.getValue() );
	    	resouceList.add( dbr );
	    	
	    	int id = agent.modifyWithID(sql, resouceList);
	    	photo.setId( id );
	    	
	    	return true;
    	} catch ( Exception e) {
    		int sd = 0;
    	}
    	
    	return false;
    }
    
    //Metodo para realizar una modificacion en la base de datos
    public boolean update(Photo photo) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();
	    	
	    	String sql = "UPDATE photos SET value = ? WHERE id = " + photo.getId() + ";";
	    	Vector<DBResource> resouceList= new Vector<DBResource>();
	    	
	    	DBResource dbr = new DBResource(ResourceType.BLOB);
	    	dbr.setValue( photo.getValue() );
	    	resouceList.add( dbr );
	    	
	    	return 1 == agent.modify(sql, resouceList);
    	} catch ( Exception e) {
    	}
    	
    	return false;
    }
    
    //Metodo para realizar una eliminacion en la base de datos
    public boolean delete(Photo photo) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();
	    	String sql = "DELETE FROM photos WHERE id =  " + photo.getId() + ";";
	    	
	    	return 1 == agent.modify( sql );
    	} catch ( Exception e) {
    	}
    	
    	return false;
    }
}
