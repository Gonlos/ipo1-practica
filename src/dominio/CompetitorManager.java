package dominio;

import java.sql.Blob;
import java.util.Date;
import java.util.Vector;

import persistencia.DBResource;
import persistencia.InterfaceAgent;
import persistencia.ResourceType;
import persistencia.SingletonAgent;

public class CompetitorManager {
    //Metodo para realizar una lectura en la base de datos
    static public Vector<Competitor> readAllBasic() {
    	try {
			InterfaceAgent agent = SingletonAgent.getAgent();
			
			String sql = "SELECT id, name, surname, photo FROM competitors;";
			
			Vector<DBResource> resouceList= new Vector<DBResource>();
			resouceList.add( new DBResource(ResourceType.INT) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.BLOB) );
			Vector<Vector<Object>> res = agent.read( sql, resouceList );
			
			Vector<Competitor> competitors = new Vector<Competitor>();
			
			for ( Vector<Object> data : res ) {				
				Competitor competitor = new Competitor();
				competitor.setId( (int) data.get(0) );
				competitor.setName( (String) data.get(1) );
				competitor.setSurname( (String) data.get(2) );	
				competitor.setPhoto( new Image( (Blob) data.get(3) ) );
				
				competitors.add( competitor );
			}
						
			return competitors;
    	} catch ( Exception e) {
    		int n = 11;
    	}
    	
    	return new Vector<Competitor>();
    }
    
    //Metodo para realizar una lectura en la base de datos
    static public Vector<Competitor> readAll( Competition competition ) {
    	try {
			InterfaceAgent agent = SingletonAgent.getAgent();
			
			String sql = "SELECT idCompetitor FROM union_Comp_Comp " + 
							"WHERE idCompetition = " + competition.getId() + ";";
			
			Vector<DBResource> resouceList= new Vector<DBResource>();
			resouceList.add( new DBResource(ResourceType.INT) );
			Vector<Vector<Object>> res = agent.read( sql, resouceList );
			
			Vector<Competitor> competitors = new Vector<Competitor>();
			
			for ( Vector<Object> data : res ) {				
				Competitor competitor = new Competitor();
				competitor.setId( (int) data.get(0) );
				competitor.read();
				
				competitors.add( competitor );
			}
						
			return competitors;
    	} catch ( Exception e) {
    		int n = 11;
    	}
    	
    	return new Vector<Competitor>();
    }
	
    //Metodo para realizar escrituras en la base de datos
    static public boolean saveAll( Competition competition ) {
		boolean noError;
		for ( Competitor competitor : competition.getCompetitors() ) {
			noError = linkCompetitionCompetitor( competition, competitor );
			if ( !noError ) return false;
		}
		
		return true;
    }
    
    static private boolean linkCompetitionCompetitor( Competition competition, Competitor competitor ) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();
	    	
	    	String sql = "INSERT INTO union_Comp_Comp( idCompetition, idCompetitor ) VALUES ( " +
	    					competition.getId() + ", " +
	    					competitor.getId() + " );";
	    	
	    	agent.modify(sql);
	    	
	    	return true;
    	} catch ( Exception e) {
    		int sd = 0;
    	}
    	
    	return false;
    }
    
    //Metodo para eliminar valores de la base de datos
    static public boolean delAll( Competition competition ) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();
	    	
	    	String sql = "DELETE FROM union_Comp_Comp WHERE idCompetition =  " + competition.getId() + ";";
	    	
	    	agent.modify(sql);
	    	
	    	return true;
    	} catch ( Exception e) {
    		int sd = 0;
    	}
    	
    	return false;
    }
	
    //Metodo para realizar una lectura en la base de datos
    public boolean read(Competitor competitor) {
    	try {
			InterfaceAgent agent = SingletonAgent.getAgent();
			
			String sql = "SELECT name, surname, dni, email, bornDate, city, telephone, shirtSize, typeRunner, photo, sponsor, club FROM competitors WHERE id = " + competitor.getId() + ";";
			
			Vector<DBResource> resouceList= new Vector<DBResource>();
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.DATE) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.BLOB) );
			resouceList.add( new DBResource(ResourceType.INT) );
			resouceList.add( new DBResource(ResourceType.INT) );
	    	
			Vector<Vector<Object>> res = agent.read( sql, resouceList );
			
			Vector<Object> data = res.get(0);
			
			competitor.setName( (String) data.get(0) );
			competitor.setSurname( (String) data.get(1) );
			competitor.setDni( (String) data.get(2) );
			competitor.setEmail( (String) data.get(3) );
			competitor.setBornDate( (Date) data.get(4) );
			competitor.setCity( (String) data.get(5) );
			competitor.setTelephone( (String) data.get(6) );
			competitor.setShirtSize( (String) data.get(7) );
			competitor.setTypeRunner( (String) data.get(8) );			
			competitor.setPhoto( new Image( (Blob) data.get(9) ) );
			
			Sponsor sponsor = new Sponsor();
			sponsor.setId( (int) data.get(10) );
			sponsor.read();
			competitor.setSponsor( sponsor );
			
			Club club = new Club();
			club.setId( (int) data.get(11) );
			club.read();
			competitor.setClub( club );
			
			return true;
    	} catch ( Exception e) {
    		int n = 11;
    	}
    	
    	return false;
    }
    
    //Metodo para realizar una insercion en la base de datos
    public boolean insert(Competitor competitor) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();
	    	String sql = "INSERT INTO competitors( name, surname, dni, email, bornDate, " + 
	    					"city, telephone, shirtSize, typeRunner, photo, sponsor, club ) VALUES ( '" +
	    					competitor.getName() + "', '" +
	    					competitor.getSurname() + "', '" +
	    					competitor.getDni() + "', '" +
	    					competitor.getEmail() + "', " +
	    					"? , '" +
	    					competitor.getCity() + "', '" +
	    					competitor.getTelephone() + "', '" +
	    					competitor.getShirtSize() + "', '" +
	    					competitor.getTypeRunner() + "', " +
	    					"? , " +
	    					competitor.getSponsor().getId() + ", " +
	    					competitor.getClub().getId() + " );";
	    	
	    	Vector<DBResource> resouceList= new Vector<DBResource>();
	    	
	    	DBResource dbr;
	    	dbr = new DBResource(ResourceType.DATE);
	    	dbr.setValue( competitor.getBornDate() );
	    	resouceList.add( dbr );
	    
	    	dbr = new DBResource(ResourceType.BLOB);
	    	dbr.setValue( competitor.getPhoto() );
	    	resouceList.add( dbr );
	    	
	    	int id = agent.modifyWithID(sql, resouceList);
	    	competitor.setId( id );
	    	
	    	return true;
    	} catch ( Exception e) {
    		int sd = 0;
    	}
    	
    	return false;
    }
    
    //Metodo para realizar una modificacion en la base de datos
    public boolean update(Competitor competitor) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();
	    	
	    	
	    	String sql = "UPDATE competitors SET " +
	    			"name = '" + competitor.getName() + "', " +
	    			"surname = '" + competitor.getSurname() + "', " +
	    			"dni = '" + competitor.getDni() + "', " +
	    			"email = '" + competitor.getEmail() + "', " +
					"bornDate = ? , " +
					"city = '" + competitor.getCity() + "', " +
					"telephone = '" + competitor.getTelephone() + "', " +
					"shirtSize = '" + competitor.getShirtSize() + "', " +
					"typeRunner = '" + competitor.getTypeRunner() + "', " +
					"photo = ? , " +
					"sponsor = " + competitor.getSponsor().getId() + ", " +
					"club = " + competitor.getClub().getId() + " " +
					"WHERE id = " + competitor.getId() + ";";
	
			Vector<DBResource> resouceList= new Vector<DBResource>();
			
			DBResource dbr;
			dbr = new DBResource(ResourceType.DATE);
			dbr.setValue( competitor.getBornDate() );
			resouceList.add( dbr );
		
			dbr = new DBResource(ResourceType.BLOB);
			dbr.setValue( competitor.getPhoto() );
			resouceList.add( dbr );
	    	
	    	return 1 == agent.modify(sql, resouceList);
    	} catch ( Exception e) {
    		int n = 11;
    	}
    	
    	return false;
    }
    
    //Metodo para realizar una eliminacion en la base de datos
    public boolean delete(Competitor competitor) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();
	    	String sql = "DELETE FROM competitors WHERE id =  " + competitor.getId() + ";";
	    	
	    	return 1 == agent.modify( sql );
    	} catch ( Exception e) {
    	}
    	
    	return false;
    }
}
