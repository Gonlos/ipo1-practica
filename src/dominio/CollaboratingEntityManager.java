package dominio;

import java.sql.Blob;
import java.util.Date;
import java.util.Vector;

import persistencia.DBResource;
import persistencia.InterfaceAgent;
import persistencia.ResourceType;
import persistencia.SingletonAgent;

public class CollaboratingEntityManager {
    //Metodo para realizar una lectura en la base de datos
    static public Vector<CollaboratingEntity> readAllBasic() {
    	try {
			InterfaceAgent agent = SingletonAgent.getAgent();
			
			String sql = "SELECT id, name, photo FROM collaboratingEntities;";
			
			Vector<DBResource> resouceList= new Vector<DBResource>();
			resouceList.add( new DBResource(ResourceType.INT) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.BLOB) );
			Vector<Vector<Object>> res = agent.read( sql, resouceList );
			
			Vector<CollaboratingEntity> collaboratingEntities = new Vector<CollaboratingEntity>();
			
			for ( Vector<Object> data : res ) {				
				CollaboratingEntity collaboratingEntity = new CollaboratingEntity();
				collaboratingEntity.setId( (int) data.get(0) );
				collaboratingEntity.setName( (String) data.get(1) );
				collaboratingEntity.setPhoto( new Image( (Blob) data.get(2) ) );
				
				collaboratingEntities.add( collaboratingEntity );
			}
						
			return collaboratingEntities;
    	} catch ( Exception e) {
    		int n = 11;
    	}
    	
    	return new Vector<CollaboratingEntity>();
    }
    
    //Metodo para realizar una lectura en la base de datos
    static public Vector<CollaboratingEntity> readAll( Competition competition ) {
    	try {
			InterfaceAgent agent = SingletonAgent.getAgent();
			
			String sql = "SELECT ce.id, ce.name, ce.photo FROM collaboratingEntities ce, union_Comp_CE u " + 
							"WHERE u.idCompetition = " + competition.getId() + " AND u.idCollaboratingEntity = ce.id;";
			
			Vector<DBResource> resouceList= new Vector<DBResource>();
			resouceList.add( new DBResource(ResourceType.INT) );
			resouceList.add( new DBResource(ResourceType.STRING) );
			resouceList.add( new DBResource(ResourceType.BLOB) );
			Vector<Vector<Object>> res = agent.read( sql, resouceList );
			
			Vector<CollaboratingEntity> collaboratingEntities = new Vector<CollaboratingEntity>();
			
			for ( Vector<Object> data : res ) {				
				CollaboratingEntity collaboratingEntity = new CollaboratingEntity();
				collaboratingEntity.setId( (int) data.get(0) );
				collaboratingEntity.setName( (String) data.get(1) );
				collaboratingEntity.setPhoto( new Image( (Blob) data.get(2) ) );
				
				collaboratingEntities.add( collaboratingEntity );
			}
						
			return collaboratingEntities;
    	} catch ( Exception e) {
    		int n = 11;
    	}
    	
    	return new Vector<CollaboratingEntity>();
    }
    
	
    //Metodo para realizar escrituras en la base de datos
    static public boolean saveAll( Competition competition ) {
		boolean noError;
		for ( CollaboratingEntity collaboratingEntity : competition.getCollaboratingEntities() ) {
			noError = linkCompetitionCompetitor( competition, collaboratingEntity );
			if ( !noError ) return false;
		}
		
		return true;
    }
    
    static private boolean linkCompetitionCompetitor( Competition competition, CollaboratingEntity collaboratingEntity ) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();
	    	
	    	String sql = "INSERT INTO union_Comp_CE( idCompetition, idCollaboratingEntity ) VALUES ( " +
	    					competition.getId() + ", " +
	    					collaboratingEntity.getId() + " );";
	    	
	    	agent.modify(sql);
	    	
	    	return true;
    	} catch ( Exception e) {
    		int sd = 0;
    	}
    	
    	return false;
    }
    
    //Metodo para eliminar valores de la base de datos
    static public boolean delAll( Competition competition ) {
    	try {
	    	InterfaceAgent agent = SingletonAgent.getAgent();
	    	
	    	String sql = "DELETE FROM union_Comp_CE WHERE idCompetition =  " + competition.getId() + ";";
	    	
	    	agent.modify(sql);
	    	
	    	return true;
    	} catch ( Exception e) {
    		int sd = 0;
    	}
    	
    	return false;
    }
	
    //Metodo para realizar una lectura en la base de datos
    public boolean read(CollaboratingEntity collaboratingEntity) {
    	return false;
    }
    
    //Metodo para realizar una insercion en la base de datos
    public boolean insert(CollaboratingEntity collaboratingEntity) {
    	return false;
    }
    
    //Metodo para realizar una modificacion en la base de datos
    public boolean update(CollaboratingEntity collaboratingEntity) {
    	return false;
    }
    
    //Metodo para realizar una eliminacion en la base de datos
    public boolean delete(CollaboratingEntity collaboratingEntity) {
    	return false;
    }
}
