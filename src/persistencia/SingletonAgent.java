package persistencia;

public class SingletonAgent {
	private static InterfaceAgent agent = null;

	public static InterfaceAgent getAgent() {
		//return AgentMySQL.getAgente();
		return AgentSQLite.getAgente();
	}
}
