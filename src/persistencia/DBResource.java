package persistencia;

public class DBResource {
	private ResourceType type;
	private Object value;
	
	public DBResource() {
		this.type = ResourceType.OBJECT;
		this.value = null;
	}
	
	public DBResource(ResourceType type) {
		this.type = type;
		this.value = null;
	}
	
	public DBResource( Object value ) {
		this.type = ResourceType.OBJECT;
		this.value = value;
	}
	
	public DBResource( ResourceType type, Object value ) {
		this.type = type;
		this.value = value;
	}
	
	public ResourceType getType() {
		return type;
	}
	
	public void setType(ResourceType type) {
		this.type = type;
	}
	
	public Object getValue() {
		return value;
	}
	
	public void setValue(Object value) {
		this.value = value;
	}
}
