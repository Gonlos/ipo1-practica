package persistencia;

public enum ResourceType {
    BLOB, DATE, STRING, INT, OBJECT, DOUBLE, BOOLEAN
}