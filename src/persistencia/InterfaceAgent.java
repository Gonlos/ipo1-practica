package persistencia;

import java.sql.*;
import java.util.Vector;

public interface InterfaceAgent {
    //Metodo para realizar una consulta en la base de datos
	public Vector<Vector<Object>> read(String SQL) throws SQLException,Exception;
	public Vector<Vector<Object>> read(String SQL, Vector<DBResource> resources) throws SQLException,Exception;
    
    // Metodo para realizar una modificacion en la base de datos
    public int modify(String SQL) throws SQLException, Exception;
    public int modify(String SQL, Vector<DBResource> resources) throws SQLException, Exception;
    
    public int modifyWithID(String SQL) throws SQLException, Exception;
    public int modifyWithID(String SQL, Vector<DBResource> resources) throws SQLException, Exception;
}