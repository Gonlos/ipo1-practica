package presentacion;

import javax.swing.JPanel;

import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

import java.awt.CardLayout;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;

import presentacion.Messages.LanguageManager;
import presentacion.Messages.Messages;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.SwingConstants;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class pnlLogin extends JPanel {
	private JTextField txtUser;
	private JPasswordField txtPassword;
	
	private JLabel lastLabel;
	JButton btnAcceder;
	
	JLabel imgESP;
	JLabel imgENG;
	
	private boolean showUserTitle;
	private boolean showPassTitle;
	
	private String userTitle;
	private String passTitle;
	private JLabel lblDatosIncorrectos;

	/**
	 * Create the panel.
	 * @param login 
	 */
	public pnlLogin(final Login login) {
		showUserTitle = true;
		showPassTitle = true;
		
		ActionListener loginListener = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if ( showPassTitle || showUserTitle ) {
					MyMessageManager.getMessageManager().showUserAndPassNeed();
					return;
				}
				
				// comprobamos si la contraseña introducida es correcta
				if ( String.valueOf(txtUser.getText()).toString().equals("Pedro") && //$NON-NLS-1$
						String.valueOf(txtPassword.getPassword()).toString().equals("1234") ) { //$NON-NLS-1$
					//Se crea una instancia de la segunda ventana (JFrame)
					Aplicacion otraVentana = new Aplicacion();
					//se hace visible
					otraVentana.setVisible(true);
					//se destruye la ventana actual (atributo a nivel de clase)
					login.dispose();
				} else {
					lblDatosIncorrectos.setVisible( true );
				}
			}
		};
		
		setPreferredSize(new Dimension(700, 480));
		setMinimumSize(new Dimension(700, 480));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 142, 41, 127, 43, -55, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 30, 0, 0, 7, 0, 7, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.anchor = GridBagConstraints.NORTHEAST;
		gbc_panel.gridwidth = 7;
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 1;
		add(panel, gbc_panel);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		imgESP = new JLabel(""); //$NON-NLS-1$
		imgESP.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				changedLabel( imgESP );
				LanguageManager.getLanguageManager().setIdioma( LanguageManager.LANG.ES );
				updateText();
			}
		});
		imgESP.setIcon(new ImageIcon(pnlLogin.class.getResource("/presentacion/images/flags/Spain.png"))); //$NON-NLS-1$
		panel.add(imgESP);
		
		imgENG = new JLabel(""); //$NON-NLS-1$
		imgENG.setEnabled(false);
		imgENG.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				changedLabel( imgENG );
				LanguageManager.getLanguageManager().setIdioma( LanguageManager.LANG.EN );
				updateText();
			}
		});
		imgENG.setIcon(new ImageIcon(pnlLogin.class.getResource("/presentacion/images/flags/United_Kingdom.png"))); //$NON-NLS-1$
		panel.add(imgENG);
		
		JLabel icon = new JLabel(""); //$NON-NLS-1$
		icon.setIcon(new ImageIcon(pnlLogin.class.getResource("/presentacion/images/Logo.png"))); //$NON-NLS-1$
		GridBagConstraints gbc_icon = new GridBagConstraints();
		gbc_icon.weighty = 0.3;
		gbc_icon.anchor = GridBagConstraints.WEST;
		gbc_icon.gridwidth = 5;
		gbc_icon.insets = new Insets(0, 0, 5, 5);
		gbc_icon.gridx = 1;
		gbc_icon.gridy = 3;
		add(icon, gbc_icon);
		
		txtUser = new JTextField();
		txtUser.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				lblDatosIncorrectos.setVisible( false );
			}
		});
		txtUser.addActionListener( loginListener );
		txtUser.setHorizontalAlignment(SwingConstants.CENTER);
		txtUser.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if ( showUserTitle ) {
					txtUser.setText(""); //$NON-NLS-1$
					showUserTitle = false;
				}
			}
			@Override
			public void focusLost(FocusEvent e) {
				if ( txtUser.getText().length() == 0 ) {
					txtUser.setText( userTitle );
					showUserTitle = true;
				}
			}
		});
		
		lblDatosIncorrectos = new JLabel();
		lblDatosIncorrectos.setForeground(Color.RED);
		lblDatosIncorrectos.setVisible(false);
		GridBagConstraints gbc_lblDatosIncorrectos = new GridBagConstraints();
		gbc_lblDatosIncorrectos.insets = new Insets(0, 0, 5, 5);
		gbc_lblDatosIncorrectos.gridx = 3;
		gbc_lblDatosIncorrectos.gridy = 4;
		add(lblDatosIncorrectos, gbc_lblDatosIncorrectos);
		
		GridBagConstraints gbc_txtUser = new GridBagConstraints();
		gbc_txtUser.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtUser.gridwidth = 3;
		gbc_txtUser.insets = new Insets(0, 0, 5, 5);
		gbc_txtUser.gridx = 2;
		gbc_txtUser.gridy = 5;
		add(txtUser, gbc_txtUser);
		txtUser.setColumns(10);
		
		txtPassword = new JPasswordField();
		txtPassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				lblDatosIncorrectos.setVisible( false );
			}
		});
		txtPassword.addActionListener( loginListener );
		txtPassword.setHorizontalAlignment(SwingConstants.CENTER);
		txtPassword.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if ( showPassTitle ) {
					txtPassword.setText(""); //$NON-NLS-1$
					txtPassword.setEchoChar('*');
					showPassTitle = false;
				}
			}
			@Override
			public void focusLost(FocusEvent e) {
				if ( txtPassword.getPassword().length == 0 ) {
					txtPassword.setEchoChar((char) 0);
					txtPassword.setText( passTitle );
					showPassTitle = true;
				}
			}
		});
		txtPassword.setEchoChar((char) 0);
		
		GridBagConstraints gbc_txtPassword = new GridBagConstraints();
		gbc_txtPassword.gridwidth = 3;
		gbc_txtPassword.insets = new Insets(0, 0, 5, 5);
		gbc_txtPassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPassword.gridx = 2;
		gbc_txtPassword.gridy = 7;
		add(txtPassword, gbc_txtPassword);
		
		btnAcceder = new JButton();
		btnAcceder.addActionListener( loginListener );
		GridBagConstraints gbc_btnAcceder = new GridBagConstraints();
		gbc_btnAcceder.insets = new Insets(0, 0, 5, 5);
		gbc_btnAcceder.gridx = 3;
		gbc_btnAcceder.gridy = 9;
		add(btnAcceder, gbc_btnAcceder);
		
		changedLabel( imgESP );
		updateText();
	}
	
	public void updateText() {
		this.userTitle = Messages.getString("pnlLogin.userTitle"); //$NON-NLS-1$
		this.passTitle = Messages.getString("pnlLogin.passwordTitle"); //$NON-NLS-1$
		
		if ( showPassTitle ) {
			txtPassword.setText( passTitle );
		}
		
		if ( showUserTitle ) {
			txtUser.setText( userTitle );
		}
		
		this.btnAcceder.setText( Messages.getString("pnlLogin.btnAcceder") ); //$NON-NLS-1$
		this.imgESP.setToolTipText(Messages.getString("pnlLogin.imgESP")); //$NON-NLS-1$
		this.imgENG.setToolTipText(Messages.getString("pnlLogin.imgENG")); //$NON-NLS-1$
		this.lblDatosIncorrectos.setText(Messages.getString("pnlLogin.loginDenied")); //$NON-NLS-1$
	}
	
	public void changedLabel( JLabel label ) {
		if ( this.lastLabel != null ) {
			this.lastLabel.setEnabled(false);
			this.lastLabel.revalidate();
		}
		this.lastLabel = label;
		this.lastLabel.setEnabled(true);
	}
}
