package presentacion.Connectors;

import java.util.Vector;

import javax.swing.JOptionPane;

import presentacion.InterfaceAppBody;
import presentacion.PnlModItem;
import presentacion.MyMessageManager;
import presentacion.AddPanels.InterfaceUpdate;
import presentacion.AddPanels.InterfaceUpdateImage;
import presentacion.Elements.CollaboratingEntityElement;
import presentacion.Elements.CompetitionElement;
import presentacion.Elements.CompetitorElement;
import presentacion.ListElements.ListAddElements;
import presentacion.ListElements.PnlAddElement;
import presentacion.ModItems.InterfaceModItem;
import presentacion.ModItems.ModItemCompetition;
import dominio.CollaboratingEntity;
import dominio.CollaboratingEntityManager;
import dominio.Competition;
import dominio.CompetitionManager;

public class ConnectorPnlAddCollaboratingEntity implements InterfaceConnectorPnlAdd {
	private InterfaceAppBody appBody;
	private InterfaceUpdateImage interfaceUpdateImage;
	
	public ConnectorPnlAddCollaboratingEntity( InterfaceAppBody appBody ) {
		this.appBody = appBody;
	}
	
	public boolean addItem() {
		MyMessageManager.getMessageManager().showWorkingMessage();
		return false;
	}
	
	public Vector<PnlAddElement> getItems( ListAddElements panelList ) {
		Vector<CollaboratingEntity> collaboratingEntities = CollaboratingEntityManager.readAllBasic();
		
		Vector<PnlAddElement> result = new Vector<PnlAddElement>();
		for ( CollaboratingEntity collaboratingEntity : collaboratingEntities ) {
			CollaboratingEntityElement collaboratingEntityElement = new CollaboratingEntityElement(collaboratingEntity, this.appBody);
			collaboratingEntityElement.setUpdate( this.interfaceUpdateImage );
			collaboratingEntityElement.setUpdateList( panelList );
			
			PnlAddElement element = new PnlAddElement(collaboratingEntityElement, panelList);
			
			result.add( element );
		}
		
		return result;
	}

	public void setUpdateImage(InterfaceUpdateImage interfaceUpdateImage) {
		this.interfaceUpdateImage = interfaceUpdateImage;
	}
}
