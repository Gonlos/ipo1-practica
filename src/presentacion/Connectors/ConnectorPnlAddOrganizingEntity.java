package presentacion.Connectors;

import java.util.Vector;

import javax.swing.JOptionPane;

import presentacion.InterfaceAppBody;
import presentacion.PnlModItem;
import presentacion.MyMessageManager;
import presentacion.AddPanels.InterfaceUpdate;
import presentacion.AddPanels.InterfaceUpdateImage;
import presentacion.Elements.CollaboratingEntityElement;
import presentacion.Elements.CompetitionElement;
import presentacion.Elements.CompetitorElement;
import presentacion.Elements.OrganizingEntityElement;
import presentacion.ListElements.ListAddElements;
import presentacion.ListElements.PnlAddElement;
import presentacion.ModItems.InterfaceModItem;
import presentacion.ModItems.ModItemCompetition;
import dominio.CollaboratingEntity;
import dominio.CollaboratingEntityManager;
import dominio.Competition;
import dominio.CompetitionManager;
import dominio.OrganizingEntity;
import dominio.OrganizingEntityManager;

public class ConnectorPnlAddOrganizingEntity implements InterfaceConnectorPnlAdd {
	private InterfaceAppBody appBody;
	private InterfaceUpdateImage interfaceUpdateImage;
	
	public ConnectorPnlAddOrganizingEntity( InterfaceAppBody appBody ) {
		this.appBody = appBody;
	}
	
	public boolean addItem() {
		MyMessageManager.getMessageManager().showWorkingMessage();
		return false;
	}
	
	public Vector<PnlAddElement> getItems( ListAddElements panelList ) {
		Vector<OrganizingEntity> organizingEntities = OrganizingEntityManager.readAllBasic();
		
		Vector<PnlAddElement> result = new Vector<PnlAddElement>();
		for ( OrganizingEntity organizingEntity : organizingEntities ) {
			OrganizingEntityElement organizingEntityElement = new OrganizingEntityElement(organizingEntity, this.appBody);
			organizingEntityElement.setUpdate( this.interfaceUpdateImage );
			organizingEntityElement.setUpdateList( panelList );
			
			PnlAddElement element = new PnlAddElement(organizingEntityElement, panelList);
			
			result.add( element );
		}
		
		return result;
	}

	public void setUpdateImage(InterfaceUpdateImage interfaceUpdateImage) {
		this.interfaceUpdateImage = interfaceUpdateImage;
	}
}
