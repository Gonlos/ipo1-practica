package presentacion.Connectors;

import java.util.Vector;

import presentacion.InterfaceAppBody;
import presentacion.MyChangePanelManager;
import presentacion.PnlModItem;
import presentacion.AddPanels.InterfaceUpdate;
import presentacion.AddPanels.InterfaceUpdateImage;
import presentacion.Elements.CompetitionElement;
import presentacion.Elements.CompetitorElement;
import presentacion.ListElements.ListAddElements;
import presentacion.ListElements.PnlAddElement;
import presentacion.ModItems.InterfaceModItem;
import presentacion.ModItems.ModItemCompetition;
import dominio.Competition;
import dominio.CompetitionManager;

public class ConnectorPnlAddCompetition implements InterfaceConnectorPnlAdd {
	private InterfaceAppBody appBody;
	private InterfaceUpdateImage interfaceUpdateImage;
	
	public ConnectorPnlAddCompetition( InterfaceAppBody appBody ) {
		this.appBody = appBody;
	}
	
	public boolean addItem() {
		MyChangePanelManager.getChangePanelManager().setCurrentState( MyChangePanelManager.STATES.LOCK_SAVE );
		Competition competition = new Competition();
		InterfaceModItem imi = new ModItemCompetition( competition );
		this.appBody.setMainPanel( new PnlModItem( imi ) );
		return true;
	}
	
	public Vector<PnlAddElement> getItems( ListAddElements panelList ) {
		Vector<Competition> competitions = CompetitionManager.readAllBasic();
		
		Vector<PnlAddElement> result = new Vector<PnlAddElement>();
		for ( Competition competition : competitions ) {
			CompetitionElement competitionElement = new CompetitionElement(competition, this.appBody);
			competitionElement.setUpdate( this.interfaceUpdateImage );
			competitionElement.setUpdateList( panelList );
			
			PnlAddElement element = new PnlAddElement(competitionElement, panelList);
			
			result.add( element );
		}
		
		return result;
	}
	
	public void setUpdateImage(InterfaceUpdateImage interfaceUpdateImage) {
		this.interfaceUpdateImage = interfaceUpdateImage;
	}
}
