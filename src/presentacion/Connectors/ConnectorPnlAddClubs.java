package presentacion.Connectors;

import java.util.Vector;

import javax.swing.JOptionPane;

import presentacion.InterfaceAppBody;
import presentacion.PnlModItem;
import presentacion.MyMessageManager;
import presentacion.AddPanels.InterfaceUpdate;
import presentacion.AddPanels.InterfaceUpdateImage;
import presentacion.Elements.ClubElement;
import presentacion.Elements.CollaboratingEntityElement;
import presentacion.Elements.CompetitionElement;
import presentacion.Elements.CompetitorElement;
import presentacion.Elements.OrganizingEntityElement;
import presentacion.Elements.SponsorElement;
import presentacion.ListElements.ListAddElements;
import presentacion.ListElements.PnlAddElement;
import presentacion.ModItems.InterfaceModItem;
import presentacion.ModItems.ModItemCompetition;
import dominio.Club;
import dominio.ClubManager;
import dominio.CollaboratingEntity;
import dominio.CollaboratingEntityManager;
import dominio.Competition;
import dominio.CompetitionManager;
import dominio.OrganizingEntity;
import dominio.OrganizingEntityManager;
import dominio.Sponsor;
import dominio.SponsorManager;

public class ConnectorPnlAddClubs implements InterfaceConnectorPnlAdd {
	private InterfaceAppBody appBody;
	private InterfaceUpdateImage interfaceUpdateImage;
	
	public ConnectorPnlAddClubs( InterfaceAppBody appBody ) {
		this.appBody = appBody;
	}
	
	public boolean addItem() {
		MyMessageManager.getMessageManager().showWorkingMessage();
		return false;
	}
	
	public Vector<PnlAddElement> getItems( ListAddElements panelList ) {
		Vector<Club> clubs = ClubManager.readAllBasic();
		
		Vector<PnlAddElement> result = new Vector<PnlAddElement>();
		for ( Club club : clubs ) {
			ClubElement clubElement = new ClubElement(club, this.appBody);
			clubElement.setUpdate( this.interfaceUpdateImage );
			clubElement.setUpdateList( panelList );
			
			PnlAddElement element = new PnlAddElement(clubElement, panelList);
			
			result.add( element );
		}
		
		return result;
	}

	public void setUpdateImage(InterfaceUpdateImage interfaceUpdateImage) {
		this.interfaceUpdateImage = interfaceUpdateImage;
	}
}
