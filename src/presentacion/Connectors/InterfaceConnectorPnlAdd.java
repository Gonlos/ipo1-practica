package presentacion.Connectors;

import java.util.Vector;

import presentacion.AddPanels.InterfaceUpdate;
import presentacion.AddPanels.InterfaceUpdateImage;
import presentacion.ListElements.ListAddElements;
import presentacion.ListElements.PnlAddElement;

public interface InterfaceConnectorPnlAdd {
	public void setUpdateImage( InterfaceUpdateImage interfaceUpdateImage );
	public boolean addItem();
	public Vector<PnlAddElement> getItems( ListAddElements panelList );
}
