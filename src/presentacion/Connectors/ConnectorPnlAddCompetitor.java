package presentacion.Connectors;

import java.util.Vector;

import presentacion.InterfaceAppBody;
import presentacion.MyChangePanelManager;
import presentacion.PnlModItem;
import presentacion.AddPanels.InterfaceUpdate;
import presentacion.AddPanels.InterfaceUpdateImage;
import presentacion.Elements.CompetitorElement;
import presentacion.ListElements.ListAddElements;
import presentacion.ListElements.PnlAddElement;
import presentacion.ModItems.InterfaceModItem;
import presentacion.ModItems.ModItemCompetitor;
import dominio.Competitor;
import dominio.CompetitorManager;

public class ConnectorPnlAddCompetitor implements InterfaceConnectorPnlAdd {
	private InterfaceAppBody appBody;
	private InterfaceUpdateImage interfaceUpdateImage;
	
	public ConnectorPnlAddCompetitor( InterfaceAppBody appBody ) {
		this.appBody = appBody;
	}
	
	public boolean addItem() {
		MyChangePanelManager.getChangePanelManager().setCurrentState( MyChangePanelManager.STATES.LOCK_SAVE );
		Competitor competitor = new Competitor();
		InterfaceModItem imi = new ModItemCompetitor( competitor );
		this.appBody.setMainPanel( new PnlModItem( imi ) );
		return true;
	}
	
	public Vector<PnlAddElement> getItems( ListAddElements panelList ) {
		Vector<Competitor> competitors = CompetitorManager.readAllBasic();
		
		Vector<PnlAddElement> result = new Vector<PnlAddElement>();
		for ( Competitor competitor : competitors ) {
			CompetitorElement competitorElement = new CompetitorElement(competitor, this.appBody);
			competitorElement.setUpdate( this.interfaceUpdateImage );
			competitorElement.setUpdateList( panelList );
			
			PnlAddElement element = new PnlAddElement(competitorElement, panelList);
			
			result.add( element );
		}
		
		return result;
	}
	
	public void setUpdateImage(InterfaceUpdateImage interfaceUpdateImage) {
		this.interfaceUpdateImage = interfaceUpdateImage;
	}
}
