package presentacion.Editor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import presentacion.Editor.Types.EditorGenericResource;

public class MyActionListener implements ActionListener {
	private GraphicEditor graphicEditor;
	private EditorGenericResource myResource;
	
	public MyActionListener( GraphicEditor graphicEditor, EditorGenericResource myResource ) {
		this.graphicEditor = graphicEditor;
		this.myResource = myResource;
	}
	
	public void actionPerformed(ActionEvent e) {
		this.graphicEditor.setSelectedResource( this.myResource );
	}
}