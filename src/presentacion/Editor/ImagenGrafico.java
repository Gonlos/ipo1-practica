package presentacion.Editor;
import java.io.Serializable;
import java.awt.Image;

import javax.swing.ImageIcon;

public class ImagenGrafico extends GraphicObject implements Serializable{
	private ImageIcon imagen;
	public ImagenGrafico(int x, int y, Image imagen) {
		super (x, y);
		this.imagen = new ImageIcon( imagen );
	}
	public void setImagen(Image imagen) { this.imagen = new ImageIcon( imagen ) ;}
	public Image getImagen() { return imagen.getImage() ;}
}
