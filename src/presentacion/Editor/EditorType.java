package presentacion.Editor;

public enum EditorType {
    NONE, IMAGE, RECT, TEXT, LINE
}