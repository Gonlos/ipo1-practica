package presentacion.Editor;
import java.awt.Color;
import java.io.Serializable;

public class RectanguloGrafico extends GraphicObject implements Serializable{
	private int x1,y1;
	private boolean xVolteada, yVolteada;
	private Color color;
	
	public RectanguloGrafico(int x, int y, int x1, int y1, Color color){
		super(x,y);
		this.x1 = x1;
		this.y1 = y1;
		xVolteada = x > x1;
		yVolteada = y > y1;
		this.color = color;
	}
	
	public void setX1(int x1) {
		if ( xVolteada ) {
			this.setX( x1 );
		} else {
			this.x1 = x1;
		}
		
		boolean b = this.getX() > x1;
		
		if ( b != xVolteada ) {
			int xOld = this.getX();
			this.setX( this.getX1() );
			this.x1 = xOld;
			xVolteada = b;
		}
	}
	
	public void setY1(int y1) {
		if ( yVolteada ) {
			this.setY( y1 );
		} else {
			this.y1 = y1;
		}
		
		boolean b = this.getY() > y1;
		
		if ( b != yVolteada ) {
			int yOld = this.getY();
			this.setY( this.getY1() );
			this.y1 = yOld;
			yVolteada = b;
		}
	}
	
	public void setColor(Color color) {this.color = color;}
	public int getX1() {return x1;}
	public int getY1() {return y1;}
	public Color getColor() {return color;}
}
