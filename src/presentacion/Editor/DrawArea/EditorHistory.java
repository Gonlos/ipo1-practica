package presentacion.Editor.DrawArea;

import java.io.Serializable;
import java.util.Vector;

import presentacion.Editor.GraphicObject;

public class EditorHistory implements Serializable {
	private Vector<GraphicObject> items;
	private int currentItemIndex;
	
	public EditorHistory() {
		this.items = new Vector<GraphicObject>();
		this.currentItemIndex = -1;
	}
	
	public boolean canBack() {
		return !(this.currentItemIndex < 0);
	}
	
	public boolean canRedo() {
		return !( this.currentItemIndex + 1 >= this.items.size() );
	}

	public boolean back( MyDrawArea drawArea ) {
		if ( this.currentItemIndex < 0 ) return false;
		
		drawArea.removeItem( this.items.get( currentItemIndex ) );
		drawArea.repaint();
		this.currentItemIndex--;
		return true;
	}
	
	public boolean redo( MyDrawArea drawArea ) {
		if ( this.currentItemIndex + 1 >= this.items.size() ) return false;
		
		this.currentItemIndex++;
		drawArea.addItem2( this.items.get( currentItemIndex ) );
		drawArea.repaint();
		return true;
	}
	
	public boolean add( MyDrawArea drawArea, GraphicObject item ) {
		this.items.setSize( this.currentItemIndex + 1 );
		this.currentItemIndex++;
		this.items.add( item );
		return drawArea.addItem2( item );
	}
}
