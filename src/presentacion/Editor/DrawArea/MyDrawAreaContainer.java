package presentacion.Editor.DrawArea;

import java.awt.Color;
import java.awt.Graphics;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import dominio.InterfaceBlob;
import presentacion.Editor.GraphicObject;
import presentacion.Editor.ImagenGrafico;
import presentacion.Editor.RectanguloGrafico;
import presentacion.Editor.TextoGrafico;

public class MyDrawAreaContainer implements InterfaceBlob {
	private MyDrawArea myDrawArea;
	
	public MyDrawAreaContainer() {
		this.myDrawArea = new MyDrawArea();
	}
	
	public MyDrawAreaContainer(MyDrawArea myDrawArea) {
		this.myDrawArea = myDrawArea;
	}

	public MyDrawArea getMyDrawArea() {
		return myDrawArea;
	}

	public void setMyDrawArea(MyDrawArea myDrawArea) {
		this.myDrawArea = myDrawArea;
	}
	
	public boolean getBlob( Blob blob ) {
		try {
		    ObjectOutputStream oos;
		    oos = new ObjectOutputStream( blob.setBinaryStream(1) );
		    oos.writeObject( this.myDrawArea );
		    oos.close();
		    
	    	return true;
		} catch ( Exception e ) {
			int n = 0;
		}
		
		return false;
	}
	
	public boolean setBlob( Blob blob ) {
		try {
			ObjectInputStream ois = null;
			ois = new ObjectInputStream( blob.getBinaryStream() );
			this.myDrawArea = (MyDrawArea) ois.readObject(); 
			
			return true;
		} catch ( Exception e ) {
			int n = 1;
		}
		
		this.myDrawArea = new MyDrawArea();
		return false;
	}
}
