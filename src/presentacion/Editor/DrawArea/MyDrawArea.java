package presentacion.Editor.DrawArea;

import java.awt.Color;
import java.awt.Graphics;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import dominio.InterfaceBlob;
import presentacion.Editor.GraphicObject;
import presentacion.Editor.ImagenGrafico;
import presentacion.Editor.LineaGrafico;
import presentacion.Editor.RectanguloGrafico;
import presentacion.Editor.TextoGrafico;

public class MyDrawArea extends JLabel implements Serializable {
	private Vector<GraphicObject> items;
	private Color color;
	private int cursorCurrentX;
	private int cursorCurrentY;
	private boolean withImage;
	
	private EditorHistory editorHistory;
	
	public MyDrawArea(){
		this.editorHistory = new EditorHistory();
		this.items = new Vector<GraphicObject>();
		this.withImage = false;
	}
	
	public boolean withImage() {
		return this.withImage;
	}
	
	@Override
	public void setIcon( Icon icon ) {
		super.setIcon(icon);
		this.editorHistory = new EditorHistory();
		this.items = new Vector<GraphicObject>();
		this.withImage = icon != null;
	}
	
	public boolean canBackAction() {
		return this.editorHistory.canBack();
	}
	
	public boolean canRedoAction() {
		return this.editorHistory.canRedo();
	}
	
	public boolean backAction() {
		return this.editorHistory.back(this);
	}
	
	public boolean redoAction() {
		return this.editorHistory.redo(this);
	}
	
	public boolean addItem( GraphicObject item ) {
		return editorHistory.add( this, item );
	}
	
	protected boolean addItem2( GraphicObject item ) {
		return this.items.add( item );
	}
	
	protected boolean removeItem( GraphicObject item ) {
		return this.items.remove( item );
	}
	
	public GraphicObject getLastItem() {
		return this.items.get( this.items.size()-1);
	}
	
	public void paint(Graphics g){
		super.paint(g);
		for ( GraphicObject graphicObject : this.items ) {
			if (graphicObject instanceof ImagenGrafico)	{
				g.drawImage(((ImagenGrafico)graphicObject).getImagen(), graphicObject.getX(),graphicObject.getY(), null);
			} else if (graphicObject instanceof RectanguloGrafico) {
				g.setColor(((RectanguloGrafico)graphicObject).getColor());
				int w = ((RectanguloGrafico)graphicObject).getX1() - graphicObject.getX();
				int h = ((RectanguloGrafico)graphicObject).getY1() - graphicObject.getY();
				g.drawRect(graphicObject.getX(),graphicObject.getY(),w,h);
			} else if (graphicObject instanceof LineaGrafico) {
				LineaGrafico lg = (LineaGrafico)graphicObject;
				g.setColor( lg.getColor() );
				g.drawLine( lg.getX(), lg.getY(), lg.getX1(), lg.getY1());
			} else {
				g.setColor(((TextoGrafico)graphicObject).getColor());
				g.drawString(((TextoGrafico)graphicObject).getTexto(),graphicObject.getX(),graphicObject.getY());
			}
			
		}
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public int getCursorCurrentX() {
		return cursorCurrentX;
	}

	public void setCursorCurrentX(int cursorCurrentX) {
		this.cursorCurrentX = cursorCurrentX;
	}

	public int getCursorCurrentY() {
		return cursorCurrentY;
	}

	public void setCursorCurrentY(int cursorCurrentY) {
		this.cursorCurrentY = cursorCurrentY;
	}
}
