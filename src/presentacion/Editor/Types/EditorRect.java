package presentacion.Editor.Types;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

import presentacion.Editor.EditorType;
import presentacion.Editor.GraphicObject;
import presentacion.Editor.RectanguloGrafico;
import presentacion.Editor.TextoGrafico;
import presentacion.Editor.DrawArea.MyDrawArea;

public class EditorRect extends EditorGenericResource {
	private JTextField txtTexto;
	
	@Override
	public EditorType getType() {
		return EditorType.RECT;
	}
	
	@Override
	public boolean addGraphicObject() {
		MyDrawArea myDrawArea = getDrawArea();
		int x = myDrawArea.getCursorCurrentX();
		int y = myDrawArea.getCursorCurrentY();
		
		myDrawArea.addItem( new RectanguloGrafico( x, y, x, y, myDrawArea.getColor() ) );
		return true;
	}
}
