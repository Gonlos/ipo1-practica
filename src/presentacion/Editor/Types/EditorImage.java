package presentacion.Editor.Types;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

import presentacion.Editor.EditorType;
import presentacion.Editor.GraphicObject;
import presentacion.Editor.ImagenGrafico;
import presentacion.Editor.RectanguloGrafico;
import presentacion.Editor.TextoGrafico;
import presentacion.Editor.DrawArea.MyDrawArea;

public class EditorImage extends EditorGenericResource {
	private JTextField txtTexto;
	
	@Override
	public EditorType getType() {
		return EditorType.IMAGE;
	}
	
	@Override
	public boolean addGraphicObject() {
		MyDrawArea myDrawArea = getDrawArea();
		int x = myDrawArea.getCursorCurrentX();
		int y = myDrawArea.getCursorCurrentY();
		
		myDrawArea.addItem( new ImagenGrafico( x, y, this.getImage() ));
		myDrawArea.repaint();
		
		return true;
	}
}
