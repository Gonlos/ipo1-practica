package presentacion.Editor.Types;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

import presentacion.Editor.EditorType;
import presentacion.Editor.GraphicObject;
import presentacion.Editor.TextoGrafico;
import presentacion.Editor.DrawArea.MyDrawArea;

public class EditorText extends EditorGenericResource {
	private JTextField txtTexto;
	
	@Override
	public EditorType getType() {
		return EditorType.TEXT;
	}
	
	@Override
	public boolean addGraphicObject() {
		this.txtTexto = new JTextField();
		MyDrawArea myDrawArea = getDrawArea();
		int x = myDrawArea.getCursorCurrentX();
		int y = myDrawArea.getCursorCurrentY();
		
		txtTexto.setBounds(x, y, 200, 30);
		txtTexto.setVisible(true);
		
		this.txtTexto.requestFocus();
		this.txtTexto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				JTextField txtTexto = (JTextField) arg.getSource();
				MyDrawArea myDrawArea = getDrawArea();
				int x = txtTexto.getBounds().x;
				int y = txtTexto.getBounds().y;
				
				if (!txtTexto.getText().equals("")) {
					myDrawArea.addItem( new TextoGrafico( x, y + 20, txtTexto.getText(), myDrawArea.getColor() ) );
				}
				
				txtTexto.setVisible(false);
				Component parent = txtTexto.getParent();
				try {
					txtTexto.getParent().remove( txtTexto );
					txtTexto.getParent().revalidate();
				} catch ( Exception e ) {
					
				}
				myDrawArea.repaint();
			}
		});

		myDrawArea.add(txtTexto);
		myDrawArea.repaint();
		return true;
	}
}
