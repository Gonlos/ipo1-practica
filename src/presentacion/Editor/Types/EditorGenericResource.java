package presentacion.Editor.Types;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.print.DocFlavor.INPUT_STREAM;
import javax.swing.ImageIcon;

import presentacion.Editor.EditorType;
import presentacion.Editor.GraphicObject;
import presentacion.Editor.DrawArea.MyDrawArea;

public class EditorGenericResource {
	private Toolkit toolkit = Toolkit.getDefaultToolkit();
	
	private Image image;
	private Cursor cursor;
	private ImageIcon imageIcon;
	private MyDrawArea drawArea;
	
	public EditorType getType() {
		return EditorType.NONE;
	}
	
	public boolean addGraphicObject() {
		return false;
	}
	
	public void readImage( URL url ) {
		this.image = toolkit.getImage( url );
	}
	
	public Image getImage() {
		return image;
	}
	
	public void setImage(Image image) {
		this.image = image;
	}
	
	public Cursor getCursor() {
		return cursor;
	}
	
	public void createCursor() {
		this.cursor = toolkit.createCustomCursor( this.image ,new Point(0,0), "Mi cursor");
	}

	public void setCursor(Cursor cursor) {
		this.cursor = cursor;
	}

	protected MyDrawArea getDrawArea() {
		return drawArea;
	}

	public ImageIcon getImageIcon() {
		return imageIcon;
	}

	public void setImageIcon(ImageIcon imageIcon) {
		this.imageIcon = imageIcon;
	}
	
	public void createImageIcon() {
		this.imageIcon = new ImageIcon( this.image );
	}

	public void setDrawArea(MyDrawArea drawArea) {
		this.drawArea = drawArea;
	}
}
