package presentacion.Editor.Types;

import presentacion.Editor.EditorType;
import presentacion.Editor.LineaGrafico;
import presentacion.Editor.DrawArea.MyDrawArea;

public class EditorLine extends EditorGenericResource {
	@Override
	public EditorType getType() {
		return EditorType.LINE;
	}
	
	@Override
	public boolean addGraphicObject() {
		MyDrawArea myDrawArea = getDrawArea();
		int x = myDrawArea.getCursorCurrentX();
		int y = myDrawArea.getCursorCurrentY();
		
		myDrawArea.addItem( new LineaGrafico( x, y, x, y, myDrawArea.getColor() ) );
		return true;
	}
}
