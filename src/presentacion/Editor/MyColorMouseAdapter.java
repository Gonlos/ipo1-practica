package presentacion.Editor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;

public class MyColorMouseAdapter extends MouseAdapter {
	private Color color;
	private GraphicEditor graphicEditor;
	
	public MyColorMouseAdapter( Color color, GraphicEditor graphicEditor ) {
		this.color = color;
		this.graphicEditor = graphicEditor;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		JLabel label = (JLabel) e.getComponent();
		this.graphicEditor.changedLabel( this.color, label );
	}
}
