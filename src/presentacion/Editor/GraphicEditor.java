package presentacion.Editor;

import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.ImageIcon;

import presentacion.Editor.DrawArea.MyDrawArea;
import presentacion.Editor.Types.EditorGenericResource;
import presentacion.Editor.Types.EditorImage;
import presentacion.Editor.Types.EditorLine;
import presentacion.Editor.Types.EditorRect;
import presentacion.Editor.Types.EditorText;
import presentacion.Messages.Messages;

import javax.swing.JScrollPane;
import javax.swing.BoxLayout;

import dominio.Competition;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import java.awt.Rectangle;
import javax.swing.border.EmptyBorder;

public class GraphicEditor extends JPanel {
	private Competition competition;
	
	private final JPanel pnlLoadImage = new JPanel();
	private final JButton btnCargarImagen = new JButton(Messages.getString("GraphicEditor.btnCargarImagen")); //$NON-NLS-1$
	private final JPanel pnlColores = new JPanel();
	
	private JLabel lastLabel;
	
	private final static Dimension lblColorDimension = new Dimension(18, 18);
	private final static Dimension lblColorDimensionSelected = new Dimension(28, 28);
	private final JLabel lblBack = new JLabel(""); //$NON-NLS-1$
	private final JLabel lblRedo = new JLabel(""); //$NON-NLS-1$
	
	private EditorGenericResource selectedResource;
	
	private Vector<EditorGenericResource> editorImages;
	private final JPanel pnlElementos = new JPanel();
	
	private ImageIcon myEditorImage;
	private final JScrollPane spImageArea = new JScrollPane();
	
	private MyDrawArea myDrawArea;
	private Cursor inCursor = null;
	private Cursor outCursor;
	private JPanel me = this;
	
	private ImageIcon backImage = new ImageIcon(GraphicEditor.class.getResource("/presentacion/Editor/Images/back_alt.png") );  //$NON-NLS-1$
	
	private ImageIcon redoImage = new ImageIcon(GraphicEditor.class.getResource("/presentacion/Editor/Images/forward_alt.png")); //$NON-NLS-1$
	private final JPanel panel = new JPanel();
	private final JScrollPane scrollPane = new JScrollPane();
	
	/**
	 * Create the panel.
	 */
	public GraphicEditor( Competition competition ) {
		this.competition = competition;
		
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				outCursor = me.getCursor();
				if ( inCursor == null ) inCursor = outCursor;
				me.setCursor(inCursor);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				inCursor = me.getCursor();
				me.setCursor(outCursor);
			}
		});
		myDrawArea = this.competition.getMyDrawArea();
		myDrawArea.addMouseMotionListener(new MyDrawAreaMouseMotionListener() );
		myDrawArea.addMouseListener( new MyDrawAreaMouseListener() );
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{60, 60, 200, 0, 0};
		gridBagLayout.rowHeights = new int[]{40, 64, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		GridBagConstraints gbc_pnlLoadImage = new GridBagConstraints();
		gbc_pnlLoadImage.gridwidth = 4;
		gbc_pnlLoadImage.insets = new Insets(0, 0, 5, 0);
		gbc_pnlLoadImage.fill = GridBagConstraints.BOTH;
		gbc_pnlLoadImage.gridx = 0;
		gbc_pnlLoadImage.gridy = 0;
		add(pnlLoadImage, gbc_pnlLoadImage);
		btnCargarImagen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fcAbrir = new JFileChooser();
				int valorDevuelto = fcAbrir.showOpenDialog( null );
				
				FileFilter ff = new FileNameExtensionFilter(Messages.getString("PnlImagePreview.imagesDescription"), "jpg", "jpeg", "png", "gif", "bmp"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
				fcAbrir.addChoosableFileFilter( ff );
				fcAbrir.setFileFilter( ff );
				
					if (valorDevuelto == JFileChooser.APPROVE_OPTION) {
						File file = fcAbrir.getSelectedFile();
						myEditorImage = new ImageIcon(file.getAbsolutePath());
						myDrawArea.setIcon(myEditorImage);
						
						updateBack();
						updateRedo();
						updatePanel();
				}
			}
		});
		
		pnlLoadImage.add(btnCargarImagen);
		
		GridBagConstraints gbc_lblBack = new GridBagConstraints();
		gbc_lblBack.insets = new Insets(0, 0, 5, 5);
		gbc_lblBack.gridx = 0;
		gbc_lblBack.gridy = 1;
		lblBack.setEnabled(false);
		lblBack.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				myDrawArea.backAction();
				updateBack();
			}
		});
		lblBack.setIcon( backImage );
		add(lblBack, gbc_lblBack);
		
		GridBagConstraints gbc_lblRedo = new GridBagConstraints();
		gbc_lblRedo.insets = new Insets(0, 0, 5, 5);
		gbc_lblRedo.gridx = 1;
		gbc_lblRedo.gridy = 1;
		lblRedo.setEnabled(false);
		lblRedo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				myDrawArea.redoAction();
				updateRedo();
			}
		});
		lblRedo.setIcon( redoImage );
		add(lblRedo, gbc_lblRedo);
		
		GridBagConstraints gbc_pnlColores = new GridBagConstraints();
		gbc_pnlColores.gridwidth = 2;
		gbc_pnlColores.insets = new Insets(0, 0, 5, 0);
		gbc_pnlColores.fill = GridBagConstraints.BOTH;
		gbc_pnlColores.gridx = 2;
		gbc_pnlColores.gridy = 1;
		pnlColores.setBorder(new TitledBorder(null, Messages.getString("GraphicEditor.TituloColores"), TitledBorder.LEADING, TitledBorder.TOP, null, null)); //$NON-NLS-1$
		add(pnlColores, gbc_pnlColores);
		
		JLabel lblColor;
		Color[] myColors = { Color.RED, Color.BLACK, Color.YELLOW, Color.BLUE, Color.GREEN, Color.WHITE, Color.CYAN, Color.ORANGE, Color.PINK };
		
		boolean first = true;
		for( Color color : myColors ) {
			lblColor = new JLabel(""); //$NON-NLS-1$
			lblColor.addMouseListener( new MyColorMouseAdapter( color, this ) );
			lblColor.setBackground( color );
			lblColor.setPreferredSize( lblColorDimension );
			lblColor.setOpaque(true);
			
			if ( first ) {
				this.changedLabel( color, lblColor );
				first = false;
			}
			
			pnlColores.add(lblColor);
		}
		
		this.editorImages = new Vector<EditorGenericResource>();
		
		GridBagConstraints gbc_spImageArea = new GridBagConstraints();
		gbc_spImageArea.gridwidth = 2;
		gbc_spImageArea.fill = GridBagConstraints.BOTH;
		gbc_spImageArea.gridx = 2;
		gbc_spImageArea.gridy = 2;
		spImageArea.setEnabled(false);
		add(spImageArea, gbc_spImageArea);
		
		spImageArea.setViewportView( myDrawArea );
		
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridwidth = 2;
		gbc_panel.insets = new Insets(0, 0, 0, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 2;
		add(panel, gbc_panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		panel.add(scrollPane);
		pnlElementos.setBorder(new TitledBorder(null, Messages.getString("GraphicEditor.TituloElementos"), TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlElementos.setLayout(new BoxLayout(pnlElementos, BoxLayout.Y_AXIS));
		
		scrollPane.setViewportView( pnlElementos );
		
		{
			URL url = getClass().getClassLoader().getResource( "presentacion/Editor/Images/texto.png" ); //$NON-NLS-1$
			EditorGenericResource editorGeneric = new EditorText();
			editorGeneric.readImage(url);
			editorGeneric.setDrawArea( this.myDrawArea );
			editorGeneric.createCursor();
			editorGeneric.createImageIcon();
			
			JButton editorButton_1 = new JButton( editorGeneric.getImageIcon() );
			editorButton_1.addActionListener( new MyActionListener(this, editorGeneric) );
			pnlElementos.add(editorButton_1);
			
			this.editorImages.add( editorGeneric );
		}
		
		{
			URL url = getClass().getClassLoader().getResource( "presentacion/Editor/Images/linea.png" ); //$NON-NLS-1$
			EditorGenericResource editorGeneric = new EditorLine();
			editorGeneric.readImage(url);
			editorGeneric.setDrawArea( this.myDrawArea );
			editorGeneric.createCursor();
			editorGeneric.createImageIcon();
			
			JButton editorButton_1 = new JButton( editorGeneric.getImageIcon() );
			editorButton_1.addActionListener( new MyActionListener(this, editorGeneric) );
			pnlElementos.add(editorButton_1);
			
			this.editorImages.add( editorGeneric );
		}		
		
		{
			URL url = getClass().getClassLoader().getResource( "presentacion/Editor/Images/cuadrado.png" ); //$NON-NLS-1$
			EditorGenericResource editorGeneric = new EditorRect();
			editorGeneric.readImage(url);
			editorGeneric.setDrawArea( this.myDrawArea );
			editorGeneric.createCursor();
			editorGeneric.createImageIcon();
			
			JButton editorButton_1 = new JButton( editorGeneric.getImageIcon() );
			editorButton_1.addActionListener( new MyActionListener(this, editorGeneric) );
			pnlElementos.add(editorButton_1);
			
			this.editorImages.add( editorGeneric );
		}		
		
		String dir = "presentacion/Editor/Elements/";
		String[] imagenes = { "Agua.png",
							  "Ambulancia.png",
							  "Bicicleta.png",
							  "Cambiador.png",
							  "Info.png",
							  "Meta.png",
							  "Parking.png",
							  "Policia.png",
							  "Salida.png",
							  "Servicio.png",
							  "Snacks.png",
							  "Telefono.png",
							  "Wifi.png" };
		
		for (String imgPath : imagenes ) {
			EditorGenericResource editorImage = new EditorImage();
			editorImage.readImage( getClass().getClassLoader().getResource( dir + imgPath ) );
			editorImage.setDrawArea(this.myDrawArea);
			editorImage.createCursor();
			editorImage.createImageIcon();

			JButton editorButton = new JButton(editorImage.getImageIcon());
			editorButton.addActionListener(new MyActionListener(this,
					editorImage));
			pnlElementos.add(editorButton);

			this.editorImages.add(editorImage);
		}
			    
		this.updateBack();
		this.updateRedo();
		this.updatePanel();
	}
	
	public void changedLabel( Color color, JLabel label ) {
		this.myDrawArea.setColor(color);
		if ( this.lastLabel != null ) {
			this.lastLabel.setPreferredSize(lblColorDimension);
			this.lastLabel.revalidate();
		}
		this.lastLabel = label;
		this.lastLabel.setPreferredSize(lblColorDimensionSelected);
	}
	
	public void setSelectedResource( EditorGenericResource myResource ) {
		this.selectedResource = myResource;
		this.setCursor( myResource.getCursor() );
	}
	
	public void updatePanel() {
		boolean b = this.myDrawArea.withImage();
		
		this.panel.setEnabled(b);
		this.panel.setVisible(b);
		this.spImageArea.setEnabled(b);
	}
	
	public void updateBack() {
		if ( myDrawArea.canBackAction() ) {
			lblRedo.setEnabled(true);
		} else {
			lblBack.setEnabled(false);
		}
	}
	
	public void updateRedo() {
		if ( myDrawArea.canRedoAction() ) {
			lblBack.setEnabled(true);
		} else {
			lblRedo.setEnabled(false);
		}
	}
	
	private class MyDrawAreaMouseListener extends MouseAdapter {
		@Override
		public void mousePressed(MouseEvent arg0) {
			myDrawArea.setCursorCurrentX( arg0.getX() );
			myDrawArea.setCursorCurrentY( arg0.getY() );
			
			if ( selectedResource != null ) {
				selectedResource.addGraphicObject();
				lblBack.setEnabled(true);
				lblRedo.setEnabled(false);
			}
		}
	}
	
	private class MyDrawAreaMouseMotionListener extends MouseMotionAdapter {
		@Override
		public void mouseDragged(MouseEvent arg0) {
			myDrawArea.setCursorCurrentX( arg0.getX() );
			myDrawArea.setCursorCurrentY( arg0.getY() );
			
			if ( selectedResource != null ) {
				switch ( selectedResource.getType() ) {
					case RECT:
						((RectanguloGrafico)myDrawArea.getLastItem()).setX1( arg0.getX() );
						((RectanguloGrafico)myDrawArea.getLastItem()).setY1( arg0.getY() );
						myDrawArea.repaint();
						break;
						
					case LINE:
						((LineaGrafico)myDrawArea.getLastItem()).setX1( arg0.getX() );
						((LineaGrafico)myDrawArea.getLastItem()).setY1( arg0.getY() );
						myDrawArea.repaint();
						break;

					default:
						break;
				}
			}
		}
	}
}
