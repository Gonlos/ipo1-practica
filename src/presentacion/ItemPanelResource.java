package presentacion;

import java.awt.CardLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class ItemPanelResource {
	private JMenuItem menu;
	private JPanel panel;
	
	private JPanel panelParent;
	
	public JMenuItem getMenu() {
		return menu;
	}
	
	public void setMenu(JMenuItem menu) {
		this.menu = menu;
	}
	
	public void newMenu( String name ) {
		this.menu = new JMenuItem( name );
		this.menu.setFont(new Font("Dialog", Font.BOLD, 11)); //$NON-NLS-1$
	}
	
	public void addToPanel( JPanel panel ) {
		this.panelParent = panel;
		
		this.menu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CardLayout cl = (CardLayout) panelParent.getLayout();
				cl.show(panelParent, arg0.getActionCommand());
			}
		});
		
		this.panelParent.add( this.panel, this.menu.getText() );
	}
	
	public JPanel getPanel() {
		return panel;
	}
	
	public void setPanel(JPanel panel) {
		this.panel = panel;
	}
}
