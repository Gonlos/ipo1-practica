package presentacion;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JPanel;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import presentacion.AddPanels.InterfaceUpdateImage;
import presentacion.Messages.Messages;
import dominio.Image;
import dominio.Photo;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.image.ImageFilter;
import java.io.File;
import java.awt.GridLayout;
import java.awt.Dimension;
import java.awt.Component;

public class PnlImagePreview extends JPanel implements InterfaceUpdateImage {
	private JButton btnCambiar;
	private Image image;
	private JLabel lblImage;
	private JLabel lblTitle;
	
	private final static int IMG_HEIGHT = 261;
	private final static int IMG_WEIGTH = 235;

	/**
	 * Create the panel.
	 */
	public PnlImagePreview() {
		this.image = new Image();
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{14, 235, 14, 0};
		gridBagLayout.rowHeights = new int[]{22, 0, 0, 261, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		lblTitle = new JLabel(Messages.getString("PnlImagePreview.lblTitle")); //$NON-NLS-1$
		GridBagConstraints gbc_lblTitle = new GridBagConstraints();
		gbc_lblTitle.anchor = GridBagConstraints.WEST;
		gbc_lblTitle.insets = new Insets(0, 0, 5, 5);
		gbc_lblTitle.gridx = 1;
		gbc_lblTitle.gridy = 1;
		add(lblTitle, gbc_lblTitle);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 1;
		gbc_panel.gridy = 3;
		add(panel, gbc_panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblImage = new JLabel(""); //$NON-NLS-1$
		lblImage.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblImage.setPreferredSize(new Dimension(IMG_WEIGTH, IMG_HEIGHT));
		lblImage.setMaximumSize(new Dimension(IMG_WEIGTH, IMG_HEIGHT));
		lblImage.setOpaque(true);
		panel.add(lblImage);
		
		btnCambiar = new JButton(Messages.getString("PnlImagePreview.btnCambiar")); //$NON-NLS-1$
		btnCambiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fcAbrir = new JFileChooser();
				FileFilter ff = new FileNameExtensionFilter(Messages.getString("PnlImagePreview.imagesDescription"), "jpg", "jpeg", "png", "gif", "bmp"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
				fcAbrir.addChoosableFileFilter( ff );
				fcAbrir.setFileFilter( ff );
				
				int valorDevuelto = fcAbrir.showOpenDialog( null );
				
				//Recoger el nombre del fichero seleccionado por el usuario
				if (valorDevuelto == JFileChooser.APPROVE_OPTION) {
					File file = fcAbrir.getSelectedFile();
					image.loadImage( file.getAbsolutePath() );
					update();
				}
			}
		});

		GridBagConstraints gbc_btnCambiar = new GridBagConstraints();
		gbc_btnCambiar.anchor = GridBagConstraints.WEST;
		gbc_btnCambiar.insets = new Insets(0, 0, 5, 5);
		gbc_btnCambiar.gridx = 1;
		gbc_btnCambiar.gridy = 5;
		add(btnCambiar, gbc_btnCambiar);

	}
	
	public void setEditable( boolean v ) {
		btnCambiar.setEnabled( v );
		btnCambiar.setVisible( v );
	}

	public void updateImage(Image image, String name) {
		this.lblTitle.setText( name );
		this.image = image;
		this.update();
	}
	
	public boolean update() {
		ImageIcon imgIcon;
		try {
//			imgIcon = new ImageIcon( this.image.getImageIcon().getImage().getScaledInstance(
//					this.lblImage.getWidth() , this.lblImage.getHeight() , java.awt.Image.SCALE_SMOOTH ) );
			ImageIcon oldImg = this.image.getImageIcon();
			double f1 = ((double) IMG_HEIGHT ) / IMG_WEIGTH;
			double f2 = ((double) oldImg.getIconHeight() ) / oldImg.getIconWidth();
			
			int h = IMG_HEIGHT;
			int w = IMG_WEIGTH;
			if ( f1 < f2 ) {
				w = (int) ( IMG_WEIGTH * (1/f2) );
			} else {
				h = (int) ( IMG_HEIGHT * f2 );
			}
			
			imgIcon = new ImageIcon( oldImg.getImage().getScaledInstance( w, h, java.awt.Image.SCALE_FAST ) );
		} catch ( Exception e ) {
			imgIcon = new ImageIcon();
		}
		
		this.lblImage.setIcon( imgIcon );
		return true;
	}
}
