package presentacion.Elements;

public interface InterfacePnlAddElement {
	public String getTitle();
	public boolean selected();
	public boolean update();
	public boolean delete();
}
