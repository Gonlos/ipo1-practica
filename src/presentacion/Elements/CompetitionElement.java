package presentacion.Elements;

import presentacion.InterfaceAppBody;
import presentacion.MyChangePanelManager;
import presentacion.MyMessageManager;
import presentacion.PnlModItem;
import presentacion.AddPanels.InterfaceUpdate;
import presentacion.AddPanels.InterfaceUpdateImage;
import presentacion.ModItems.InterfaceModItem;
import presentacion.ModItems.ModItemCompetition;
import presentacion.ModItems.ModItemCompetitor;
import dominio.Competition;
import dominio.Competitor;
import dominio.Image;

public class CompetitionElement implements InterfacePnlAddElement {
	private Competition competition;
	private InterfaceAppBody appBody;
	private InterfaceUpdateImage update;
	private InterfaceUpdate updateList;
	
	public CompetitionElement(Competition competition, InterfaceAppBody appBody ) {
		this.competition = competition;
		this.appBody = appBody;
		this.update = null;
	}
	
	public String getTitle() {
		return this.competition.toString();
	}
	
	public boolean selected() {
		if ( this.update != null) this.update.updateImage( this.competition.getPoster(), this.competition.toString() );
		return true;
	}
	
	public boolean update() {
		MyChangePanelManager.getChangePanelManager().setCurrentState( MyChangePanelManager.STATES.LOCK_SAVE );
		this.competition.read();
		InterfaceModItem imi = new ModItemCompetition( this.competition );
		this.appBody.setMainPanel( new PnlModItem( imi ) );
		return true;
	}
	
	public boolean delete() {
		if ( MyMessageManager.getMessageManager().showConfirmDeleteMessage( this.getTitle() ) ) {
			boolean b = competition.delete();
			this.updateList.update();
			return b;
		}
		
		return false;
	}

	public void setUpdate(InterfaceUpdateImage update) {
		this.update = update;
	}
	
	public void setUpdateList(InterfaceUpdate updateList) {
		this.updateList = updateList;
	}
}
