package presentacion.Elements;

import presentacion.InterfaceAppBody;
import presentacion.MyChangePanelManager;
import presentacion.MyMessageManager;
import presentacion.PnlModItem;
import presentacion.AddPanels.InterfaceUpdate;
import presentacion.AddPanels.InterfaceUpdateImage;
import presentacion.ModItems.InterfaceModItem;
import presentacion.ModItems.ModItemCompetitor;
import dominio.Competitor;

public class CompetitorElement implements InterfacePnlAddElement {
	private Competitor competitor;
	private InterfaceAppBody appBody;
	private InterfaceUpdateImage update;
	private InterfaceUpdate updateList;
	
	public CompetitorElement(Competitor competitor, InterfaceAppBody appBody) {
		this.competitor = competitor;
		this.appBody = appBody;
	}
	
	public String getTitle() {
		return competitor.toString();
	}
	
	public boolean selected() {
		if ( this.update != null) this.update.updateImage( this.competitor.getPhoto(), this.competitor.toString() );
		return true;
	}
	
	public boolean update() {
		MyChangePanelManager.getChangePanelManager().setCurrentState( MyChangePanelManager.STATES.LOCK_SAVE );
		this.competitor.read();
		InterfaceModItem imi = new ModItemCompetitor( this.competitor );
		this.appBody.setMainPanel( new PnlModItem( imi ) );
		return true;
	}
	
	public boolean delete() {
		if ( MyMessageManager.getMessageManager().showConfirmDeleteMessage( this.getTitle() ) ) {
			boolean b = competitor.delete();
			this.updateList.update();
			return b;
		}
		
		return false;
	}
	
	public void setUpdate(InterfaceUpdateImage update) {
		this.update = update;
	}
	
	public void setUpdateList(InterfaceUpdate updateList) {
		this.updateList = updateList;
	}
}
