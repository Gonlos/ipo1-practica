package presentacion.Elements;

import presentacion.InterfaceAppBody;
import presentacion.PnlModItem;
import presentacion.MyMessageManager;
import presentacion.AddPanels.InterfaceUpdate;
import presentacion.AddPanels.InterfaceUpdateImage;
import presentacion.ModItems.InterfaceModItem;
import presentacion.ModItems.ModItemCompetition;
import presentacion.ModItems.ModItemCompetitor;
import dominio.CollaboratingEntity;
import dominio.Competition;
import dominio.Competitor;
import dominio.Image;

public class CollaboratingEntityElement implements InterfacePnlAddElement {
	private CollaboratingEntity collaboratingEntity;
	private InterfaceAppBody appBody;
	private InterfaceUpdateImage update;
	private InterfaceUpdate updateList;
	
	public CollaboratingEntityElement(CollaboratingEntity collaboratingEntity, InterfaceAppBody appBody ) {
		this.collaboratingEntity = collaboratingEntity;
		this.appBody = appBody;
		this.updateList = null;
		this.update = null;
	}
	
	public String getTitle() {
		return this.collaboratingEntity.toString();
	}
	
	public boolean selected() {
		if ( this.update != null) this.update.updateImage( this.collaboratingEntity.getPhoto(), this.collaboratingEntity.toString() );
		return true;
	}
	
	public boolean update() {
		MyMessageManager.getMessageManager().showWorkingMessage();
		return false;
	}
	
	public boolean delete() {
		MyMessageManager.getMessageManager().showWorkingMessage();
		return false;
	}

	public void setUpdate(InterfaceUpdateImage update) {
		this.update = update;
	}

	public void setUpdateList(InterfaceUpdate updateList) {
		this.updateList = updateList;
	}
}
