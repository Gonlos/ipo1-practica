package presentacion.ListElements;

import javax.swing.JPanel;

import java.awt.GridLayout;

import javax.swing.JLabel;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.ImageIcon;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Dimension;

import javax.swing.border.LineBorder;

import presentacion.Elements.InterfacePnlAddElement;
import presentacion.Messages.Messages;

import java.awt.Color;

public class PnlAddElement extends JPanel {
	private InterfacePnlAddElement panelElement;

	private ListAddElements panelList;
	
	final static Color COLOR_SELECTED = new Color(180, 210, 255);
	final static Color COLOR_NO_SELECTED = new Color(238, 238, 238);
	
	/**
	 * Create the panel.
	 */
	public PnlAddElement( InterfacePnlAddElement element, ListAddElements panelList ) {
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				itemSelected();
			}
			@Override
			public void mouseExited(MouseEvent e) {
				itemNoSelected();
			}
			@Override
			public void mouseClicked(MouseEvent e) {
				panelElement.update();
			}
		});
		setBorder(null);
		setMaximumSize(new Dimension(32767, 24));
		this.panelElement = element;
		this.panelList = panelList;
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{293, 14, 0, 0};
		gridBagLayout.rowHeights = new int[] {24, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0};
		setLayout(gridBagLayout);
		
		JLabel lblItemName = new JLabel( this.panelElement.getTitle() );
		GridBagConstraints gbc_lblItemName = new GridBagConstraints();
		gbc_lblItemName.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblItemName.insets = new Insets(0, 0, 5, 5);
		gbc_lblItemName.gridx = 0;
		gbc_lblItemName.gridy = 0;
		add(lblItemName, gbc_lblItemName);
		
		final JLabel imgMod = new JLabel(""); //$NON-NLS-1$
		imgMod.setToolTipText(Messages.getString("PnlAddElement.ToolTipImgMod")); //$NON-NLS-1$
		imgMod.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				panelElement.update();
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				imgMod.setIcon(new ImageIcon(PnlAddElement.class.getResource("/presentacion/images/icons/Rename Document (selected).png"))); //$NON-NLS-1$
				itemSelected();
			}
			@Override
			public void mouseExited(MouseEvent e) {
				imgMod.setIcon(new ImageIcon(PnlAddElement.class.getResource("/presentacion/images/icons/Rename Document.png"))); //$NON-NLS-1$
				itemNoSelected();
			}
		});
		imgMod.setIcon(new ImageIcon(PnlAddElement.class.getResource("/presentacion/images/icons/Rename Document.png"))); //$NON-NLS-1$
		GridBagConstraints gbc_imgMod = new GridBagConstraints();
		gbc_imgMod.anchor = GridBagConstraints.EAST;
		gbc_imgMod.insets = new Insets(0, 0, 5, 5);
		gbc_imgMod.gridx = 1;
		gbc_imgMod.gridy = 0;
		add(imgMod, gbc_imgMod);
		
		final JLabel imgDel = new JLabel(""); //$NON-NLS-1$
		imgDel.setToolTipText(Messages.getString("PnlAddElement.ToolTipImgDel")); //$NON-NLS-1$
		imgDel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ( panelElement.delete() ) {
					removeFromList();
				}
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				imgDel.setIcon(new ImageIcon(PnlAddElement.class.getResource("/presentacion/images/icons/Remove Document (selected).png"))); //$NON-NLS-1$
				itemSelected();
			}
			@Override
			public void mouseExited(MouseEvent e) {
				imgDel.setIcon(new ImageIcon(PnlAddElement.class.getResource("/presentacion/images/icons/Remove Document.png"))); //$NON-NLS-1$
				itemNoSelected();
			}
		});
		imgDel.setIcon(new ImageIcon(PnlAddElement.class.getResource("/presentacion/images/icons/Remove Document.png"))); //$NON-NLS-1$
		GridBagConstraints gbc_imgDel = new GridBagConstraints();
		gbc_imgDel.insets = new Insets(0, 0, 5, 0);
		gbc_imgDel.gridx = 2;
		gbc_imgDel.gridy = 0;
		add(imgDel, gbc_imgDel);

	}
	
	private void itemSelected() {
		this.panelElement.selected();
		this.setBackground( COLOR_SELECTED );
	}
	
	private void itemNoSelected() {
		this.setBackground( COLOR_NO_SELECTED );
	}

	public void removeFromList() {
		this.panelList.removeItem( this );
	}
}
