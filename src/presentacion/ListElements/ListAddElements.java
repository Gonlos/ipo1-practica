package presentacion.ListElements;

import java.awt.Component;
import java.awt.GridLayout;
import java.util.Vector;

import javax.swing.JPanel;

import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JScrollPane;

import presentacion.AddPanels.InterfaceUpdate;

public class ListAddElements extends JPanel implements InterfaceUpdate {
	final static int VSIZE_PANEL = 5;
	private final JPanel panel = new JPanel();
	
	/**
	 * Create the panel.
	 */
	public ListAddElements() {
		setAlignmentY(Component.TOP_ALIGNMENT);
		setAlignmentX(Component.LEFT_ALIGNMENT);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{474, 0};
		gridBagLayout.rowHeights = new int[]{24, -63, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		add(panel, gbc_panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
	}
	
	public void addItems( Vector<PnlAddElement> elements ) {
		for ( PnlAddElement element : elements ) {
			this.panel.add(element);
		}
		
		this.update();
	}

	public void addItem( PnlAddElement element ) {
		this.panel.add( element );
		this.update();
	}
	
	public void removeItem( PnlAddElement element ) {
		this.panel.remove( element );
		this.update();
	}
	
	@Override
	public boolean update() {
		this.panel.setSize( this.panel.getWidth(), this.panel.getComponentCount() * VSIZE_PANEL );
		this.panel.revalidate();
		this.panel.repaint();
		return true;
	}
}
