package presentacion.Messages;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import presentacion.MyMessageManager;

public class LanguageManager {
	public static enum LANG {
		ES, EN
	}
	
	private String bundleName = "presentacion.Messages.messages"; //$NON-NLS-1$
	private ResourceBundle resourceBundle = ResourceBundle.getBundle(bundleName);
	
	static private LanguageManager mInstancia;

	private LanguageManager() {
	}
	
	public static LanguageManager getLanguageManager() {
		try {
			if (mInstancia == null) {
				mInstancia = new LanguageManager();
			}

			return mInstancia;
		} catch (Exception e) {
		}

		return null;
	}

	public String getString(String key) {
		try {
			return resourceBundle.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
	
	private Locale getLocale( LANG lang ) {
		Locale locale = new Locale( lang.toString().toLowerCase() );
		return locale;
	}

	// Obtiene el idioma
	public void setIdioma( LANG lang ) {
		this.resourceBundle = ResourceBundle.getBundle( this.bundleName, this.getLocale( lang ) );
	}
}
