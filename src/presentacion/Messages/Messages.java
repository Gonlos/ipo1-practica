package presentacion.Messages;

public class Messages {
	private Messages() {
	}

	public static String getString(String key) {
		return LanguageManager.getLanguageManager().getString(key);
	}
}
