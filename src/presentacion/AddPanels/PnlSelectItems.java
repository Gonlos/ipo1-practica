package presentacion.AddPanels;

import javax.swing.JPanel;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Font;

import javax.swing.DefaultListModel;
import javax.swing.DropMode;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.TransferHandler;

import presentacion.AddPanels.ListTransferHandler.BiTransferHandler;
import presentacion.AddPanels.ListTransferHandler.FromTransferHandler;
import presentacion.AddPanels.ListTransferHandler.ToTransferHandler;
import presentacion.Messages.Messages;
import presentacion.SelectedItems.InterfaceSelectedItems;

import java.awt.Color;

import javax.swing.AbstractListModel;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class PnlSelectItems extends JPanel {
	private InterfaceSelectedItems selectedItems;
	private JLabel lblNumRemainigItems;
	
	private JList listAvailableItems;
	private JList listSelectedItems;
	
	/**
	 * Create the panel.
	 */
	public PnlSelectItems( InterfaceSelectedItems mySelectedItems ) {
		this.selectedItems = mySelectedItems;
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{20, 188, 18, 0, 20, 0};
		gridBagLayout.rowHeights = new int[]{16, 0, 0, 16, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblAvailableItems = new JLabel( this.selectedItems.getTitleNoChosenItems() );
		GridBagConstraints gbc_lblAvailableItems = new GridBagConstraints();
		gbc_lblAvailableItems.anchor = GridBagConstraints.WEST;
		gbc_lblAvailableItems.insets = new Insets(0, 0, 5, 5);
		gbc_lblAvailableItems.gridx = 1;
		gbc_lblAvailableItems.gridy = 1;
		add(lblAvailableItems, gbc_lblAvailableItems);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 3;
		gbc_panel.gridy = 1;
		add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0};
		gbl_panel.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblSelectedItems = new JLabel( this.selectedItems.getTitleChosenItems() );
		GridBagConstraints gbc_lblSelectedItems = new GridBagConstraints();
		gbc_lblSelectedItems.anchor = GridBagConstraints.WEST;
		gbc_lblSelectedItems.insets = new Insets(0, 0, 0, 5);
		gbc_lblSelectedItems.gridx = 0;
		gbc_lblSelectedItems.gridy = 0;
		panel.add(lblSelectedItems, gbc_lblSelectedItems);
		
		lblNumRemainigItems = new JLabel(""); //$NON-NLS-1$
		GridBagConstraints gbc_lblNumRemainigItems = new GridBagConstraints();
		gbc_lblNumRemainigItems.anchor = GridBagConstraints.EAST;
		gbc_lblNumRemainigItems.gridx = 1;
		gbc_lblNumRemainigItems.gridy = 0;
		panel.add(lblNumRemainigItems, gbc_lblNumRemainigItems);
		lblNumRemainigItems.setFont(new Font("Dialog", Font.BOLD, 10)); //$NON-NLS-1$
		
		JScrollPane spAvailableItems = new JScrollPane();
		GridBagConstraints gbc_spAvailableItems = new GridBagConstraints();
		gbc_spAvailableItems.insets = new Insets(0, 0, 5, 5);
		gbc_spAvailableItems.fill = GridBagConstraints.BOTH;
		gbc_spAvailableItems.gridx = 1;
		gbc_spAvailableItems.gridy = 2;
		add(spAvailableItems, gbc_spAvailableItems);
		
		listAvailableItems = new JList();
		MyListModel lmAvailableItems = new MyListModel( this.selectedItems.getNoChosenItems() );
		listAvailableItems.setModel( lmAvailableItems );
		spAvailableItems.setViewportView(listAvailableItems);
		
		//cbPatrocinador.setModel(new DefaultComboBoxModel( SponsorManager.readAllBasic() ) );
		
		JScrollPane spSelectedItems = new JScrollPane();
		GridBagConstraints gbc_spSelectedItems = new GridBagConstraints();
		gbc_spSelectedItems.insets = new Insets(0, 0, 5, 5);
		gbc_spSelectedItems.fill = GridBagConstraints.BOTH;
		gbc_spSelectedItems.gridx = 3;
		gbc_spSelectedItems.gridy = 2;
		add(spSelectedItems, gbc_spSelectedItems);
		
		listSelectedItems = new JList();	
		MyListModel lmSelectedItems = new MyListModel( this.selectedItems.getChosenItems() );
		InterfaceUpdate updateList = new UpdateList(this, mySelectedItems, lmSelectedItems );
		lmSelectedItems.setUpdate(updateList);
		listSelectedItems.setModel( lmSelectedItems );
		spSelectedItems.setViewportView(listSelectedItems);		
		
		// set the drag&drop list function
		
//		this.listAvailableItems.setTransferHandler(new FromTransferHandler( lmAvailableItems, this.listAvailableItems ));
//		this.listAvailableItems.setDragEnabled(true);
//		this.listAvailableItems.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//		
//		this.listSelectedItems.setTransferHandler(new ToTransferHandler(TransferHandler.MOVE));
//		this.listSelectedItems.setDropMode(DropMode.INSERT);
		
		this.listAvailableItems.setTransferHandler(new BiTransferHandler( lmAvailableItems, this.listAvailableItems, TransferHandler.MOVE ));
		this.listAvailableItems.setDragEnabled(true);
		this.listAvailableItems.setDropMode(DropMode.INSERT);
		this.listAvailableItems.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		this.listSelectedItems.setTransferHandler(new BiTransferHandler( lmSelectedItems, this.listSelectedItems, TransferHandler.MOVE ));
		this.listSelectedItems.setDragEnabled(true);
		this.listSelectedItems.setDropMode(DropMode.INSERT);
		this.listSelectedItems.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		this.updateNumElements();
	}
	
	public void updateNumElements() {
		boolean seguir = this.selectedItems.isLimitedItems();
		this.lblNumRemainigItems.setVisible( seguir );
		
		if ( seguir ) {
			int max = this.selectedItems.getMaxNumItems();
			int actual = this.listSelectedItems.getModel().getSize();
			
			this.lblNumRemainigItems.setText( String.format(Messages.getString("PnlSelectItems.lblNumRemainigItems"),  actual, max ) ); //$NON-NLS-1$
			
			double f = ( max == 0 ) ? 1.0 : (((double) actual) / max);
			Color color = Color.GREEN;
			if ( f > 0.9 ) {
				color = Color.RED;
			} else if ( f > 0.6 ) {
				color = Color.ORANGE;
			}
			
			this.lblNumRemainigItems.setForeground(color);
			
		}
	}

}
