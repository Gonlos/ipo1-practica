package presentacion.AddPanels;

import javax.swing.JPanel;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JTextField;
import javax.swing.JEditorPane;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JTextArea;
import javax.swing.JCheckBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.MaskFormatter;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import presentacion.Messages.Messages;
import dominio.CityList;
import dominio.Club;
import dominio.ClubManager;
import dominio.Competitor;
import dominio.CompetitorManager;
import dominio.Sponsor;
import dominio.SponsorManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.text.ParseException;
import java.util.Date;
import java.util.Properties;

public class InfoParticipante extends JPanel {
	private JTextField tfNombre;
	private JTextField tfApellidos;
	private JFormattedTextField ftfDNI;
	private JFormattedTextField ftfCorreoElectronico;
	private JComboBox cbLocalidad;
	private JFormattedTextField ftfTelefono;
	private JComboBox cbTallaCamiseta;
	private JComboBox cbTipoCorredor;
	private JComboBox cbPatrocinador;
	private JComboBox cbClubDeportivo;
	private JDatePickerImpl dpFechaNacimiento;
	
	private Competitor competitor;

	/**
	 * Create the panel.
	 */
	public InfoParticipante( Competitor myCompetitor ) {
		this.competitor = myCompetitor;
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{20, 0, 145, 0, 20, 0};
		gridBagLayout.rowHeights = new int[]{16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblNombre = new JLabel(Messages.getString("InfoParticipante.lblNombre")); //$NON-NLS-1$
		GridBagConstraints gbc_lblNombre = new GridBagConstraints();
		gbc_lblNombre.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblNombre.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombre.gridx = 1;
		gbc_lblNombre.gridy = 1;
		add(lblNombre, gbc_lblNombre);
		
		tfNombre = new JTextField();		
		tfNombre.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void insertUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void removeUpdate(DocumentEvent arg0) { warn(); }
			
			public void warn() {
				competitor.setName( tfNombre.getText() );
			}
		});
		
		GridBagConstraints gbc_tfNombre = new GridBagConstraints();
		gbc_tfNombre.gridwidth = 2;
		gbc_tfNombre.insets = new Insets(0, 0, 5, 5);
		gbc_tfNombre.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfNombre.gridx = 2;
		gbc_tfNombre.gridy = 1;
		add(tfNombre, gbc_tfNombre);
		tfNombre.setColumns(10);
		
		JLabel lblApellidos = new JLabel(Messages.getString("InfoParticipante.lblApellidos")); //$NON-NLS-1$
		GridBagConstraints gbc_lblApellidos = new GridBagConstraints();
		gbc_lblApellidos.anchor = GridBagConstraints.WEST;
		gbc_lblApellidos.insets = new Insets(0, 0, 5, 5);
		gbc_lblApellidos.gridx = 1;
		gbc_lblApellidos.gridy = 2;
		add(lblApellidos, gbc_lblApellidos);
		
		tfApellidos = new JTextField();
		tfApellidos.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void insertUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void removeUpdate(DocumentEvent arg0) { warn(); }
			
			public void warn() {
				competitor.setSurname( tfApellidos.getText() );
			}
		});

		GridBagConstraints gbc_tfApellidos = new GridBagConstraints();
		gbc_tfApellidos.gridwidth = 2;
		gbc_tfApellidos.insets = new Insets(0, 0, 5, 5);
		gbc_tfApellidos.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfApellidos.gridx = 2;
		gbc_tfApellidos.gridy = 2;
		add(tfApellidos, gbc_tfApellidos);
		tfApellidos.setColumns(10);
		
		JLabel lblDni = new JLabel(Messages.getString("InfoParticipante.lblDni")); //$NON-NLS-1$
		GridBagConstraints gbc_lblDni = new GridBagConstraints();
		gbc_lblDni.anchor = GridBagConstraints.WEST;
		gbc_lblDni.insets = new Insets(0, 0, 5, 5);
		gbc_lblDni.gridx = 1;
		gbc_lblDni.gridy = 3;
		add(lblDni, gbc_lblDni);
		
		try {
			MaskFormatter formato = new MaskFormatter("########'-U"); //$NON-NLS-1$
			formato.setPlaceholderCharacter('*');
			ftfDNI = new JFormattedTextField( formato );
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ftfDNI.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void insertUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void removeUpdate(DocumentEvent arg0) { warn(); }
			
			public void warn() {
				competitor.setDni( ftfDNI.getText() );
			}
		});
		
		GridBagConstraints gbc_ftfDNI = new GridBagConstraints();
		gbc_ftfDNI.insets = new Insets(0, 0, 5, 5);
		gbc_ftfDNI.fill = GridBagConstraints.HORIZONTAL;
		gbc_ftfDNI.gridx = 2;
		gbc_ftfDNI.gridy = 3;
		add(ftfDNI, gbc_ftfDNI);
		
		JLabel lblCorreoElectornico = new JLabel(Messages.getString("InfoParticipante.lblCorreoElectornico")); //$NON-NLS-1$
		GridBagConstraints gbc_lblCorreoElectornico = new GridBagConstraints();
		gbc_lblCorreoElectornico.anchor = GridBagConstraints.WEST;
		gbc_lblCorreoElectornico.insets = new Insets(0, 0, 5, 5);
		gbc_lblCorreoElectornico.gridx = 1;
		gbc_lblCorreoElectornico.gridy = 4;
		add(lblCorreoElectornico, gbc_lblCorreoElectornico);
		
		ftfCorreoElectronico = new JFormattedTextField( new RegexPatternFormatter( "\\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}\\b" ) ); //$NON-NLS-1$
		ftfCorreoElectronico.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void insertUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void removeUpdate(DocumentEvent arg0) { warn(); }
			
			public void warn() {
				competitor.setEmail( ftfCorreoElectronico.getText() );
			}
		});

		GridBagConstraints gbc_ftfCorreoElectronico = new GridBagConstraints();
		gbc_ftfCorreoElectronico.gridwidth = 2;
		gbc_ftfCorreoElectronico.insets = new Insets(0, 0, 5, 5);
		gbc_ftfCorreoElectronico.fill = GridBagConstraints.HORIZONTAL;
		gbc_ftfCorreoElectronico.gridx = 2;
		gbc_ftfCorreoElectronico.gridy = 4;
		add(ftfCorreoElectronico, gbc_ftfCorreoElectronico);
		
		JLabel lblFechaNacimiento = new JLabel(Messages.getString("InfoParticipante.lblFechaNacimiento")); //$NON-NLS-1$
		GridBagConstraints gbc_lblFechaNacimiento = new GridBagConstraints();
		gbc_lblFechaNacimiento.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblFechaNacimiento.insets = new Insets(0, 0, 5, 5);
		gbc_lblFechaNacimiento.gridx = 1;
		gbc_lblFechaNacimiento.gridy = 5;
		add(lblFechaNacimiento, gbc_lblFechaNacimiento);
		
		UtilDateModel model = new MyDateModel();
		JDatePanelImpl datePanel = new JDatePanelImpl(model, new Properties());
		dpFechaNacimiento = new JDatePickerImpl(datePanel, new DateLabelFormatter() );
		dpFechaNacimiento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Date selectedDate = (Date) dpFechaNacimiento.getModel().getValue();
				competitor.setBornDate(selectedDate);
			}
		});
		GridBagConstraints gbc_dpFechaNacimiento = new GridBagConstraints();
		gbc_dpFechaNacimiento.insets = new Insets(0, 0, 5, 5);
		gbc_dpFechaNacimiento.gridx = 2;
		gbc_dpFechaNacimiento.gridy = 5;
		add(dpFechaNacimiento, gbc_dpFechaNacimiento);
		
		JLabel lblLocalidad = new JLabel(Messages.getString("InfoParticipante.lblLocalidad")); //$NON-NLS-1$
		GridBagConstraints gbc_lblLocalidad = new GridBagConstraints();
		gbc_lblLocalidad.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblLocalidad.insets = new Insets(0, 0, 5, 5);
		gbc_lblLocalidad.gridx = 1;
		gbc_lblLocalidad.gridy = 6;
		add(lblLocalidad, gbc_lblLocalidad);
		
		cbLocalidad = new JComboBox();
		cbLocalidad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				competitor.setCity( (String) cbLocalidad.getSelectedItem() );
			}
		});
		cbLocalidad.setModel(new DefaultComboBoxModel( CityList.CITY_LIST ) );
		GridBagConstraints gbc_cbLocalidad = new GridBagConstraints();
		gbc_cbLocalidad.gridwidth = 2;
		gbc_cbLocalidad.insets = new Insets(0, 0, 5, 5);
		gbc_cbLocalidad.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbLocalidad.gridx = 2;
		gbc_cbLocalidad.gridy = 6;
		add(cbLocalidad, gbc_cbLocalidad);
		
		JLabel lblTelfono = new JLabel(Messages.getString("InfoParticipante.lblTelfono")); //$NON-NLS-1$
		GridBagConstraints gbc_lblTelfono = new GridBagConstraints();
		gbc_lblTelfono.anchor = GridBagConstraints.WEST;
		gbc_lblTelfono.insets = new Insets(0, 0, 5, 5);
		gbc_lblTelfono.gridx = 1;
		gbc_lblTelfono.gridy = 7;
		add(lblTelfono, gbc_lblTelfono);
		
		try {
			MaskFormatter formato = new MaskFormatter("'(###')' ###' ##' ##' ##"); //$NON-NLS-1$
			formato.setPlaceholderCharacter('*');
			ftfTelefono = new JFormattedTextField( formato );
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ftfTelefono.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void insertUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void removeUpdate(DocumentEvent arg0) { warn(); }
			
			public void warn() {
				competitor.setTelephone( ftfTelefono.getText() );
			}
		});

		GridBagConstraints gbc_ftfTelefono = new GridBagConstraints();
		gbc_ftfTelefono.insets = new Insets(0, 0, 5, 5);
		gbc_ftfTelefono.fill = GridBagConstraints.HORIZONTAL;
		gbc_ftfTelefono.gridx = 2;
		gbc_ftfTelefono.gridy = 7;
		add(ftfTelefono, gbc_ftfTelefono);
		
		JLabel lblTallaDeCamiseta = new JLabel(Messages.getString("InfoParticipante.lblTallaDeCamiseta")); //$NON-NLS-1$
		GridBagConstraints gbc_lblTallaDeCamiseta = new GridBagConstraints();
		gbc_lblTallaDeCamiseta.anchor = GridBagConstraints.WEST;
		gbc_lblTallaDeCamiseta.insets = new Insets(0, 0, 5, 5);
		gbc_lblTallaDeCamiseta.gridx = 1;
		gbc_lblTallaDeCamiseta.gridy = 8;
		add(lblTallaDeCamiseta, gbc_lblTallaDeCamiseta);
		
		cbTallaCamiseta = new JComboBox();
		cbTallaCamiseta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				competitor.setShirtSize( (String) cbTallaCamiseta.getSelectedItem() );
			}
		});
		cbTallaCamiseta.setModel(new DefaultComboBoxModel(new String[] {"XXL", "XL", "L", "M", "S"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		GridBagConstraints gbc_cbTallaCamiseta = new GridBagConstraints();
		gbc_cbTallaCamiseta.gridwidth = 2;
		gbc_cbTallaCamiseta.insets = new Insets(0, 0, 5, 5);
		gbc_cbTallaCamiseta.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbTallaCamiseta.gridx = 2;
		gbc_cbTallaCamiseta.gridy = 8;
		add(cbTallaCamiseta, gbc_cbTallaCamiseta);
		
		JLabel lblTipoDeCorredor = new JLabel(Messages.getString("InfoParticipante.lblTipoDeCorredor")); //$NON-NLS-1$
		GridBagConstraints gbc_lblTipoDeCorredor = new GridBagConstraints();
		gbc_lblTipoDeCorredor.anchor = GridBagConstraints.WEST;
		gbc_lblTipoDeCorredor.insets = new Insets(0, 0, 5, 5);
		gbc_lblTipoDeCorredor.gridx = 1;
		gbc_lblTipoDeCorredor.gridy = 9;
		add(lblTipoDeCorredor, gbc_lblTipoDeCorredor);
		
		cbTipoCorredor = new JComboBox();
		cbTipoCorredor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				competitor.setTypeRunner( (String) cbTipoCorredor.getSelectedItem() );
			}
		});
		cbTipoCorredor.setModel(new DefaultComboBoxModel(new String[] {"Amateur", "Senior"})); //$NON-NLS-1$ //$NON-NLS-2$
		GridBagConstraints gbc_cbTipoCorredor = new GridBagConstraints();
		gbc_cbTipoCorredor.gridwidth = 2;
		gbc_cbTipoCorredor.insets = new Insets(0, 0, 5, 5);
		gbc_cbTipoCorredor.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbTipoCorredor.gridx = 2;
		gbc_cbTipoCorredor.gridy = 9;
		add(cbTipoCorredor, gbc_cbTipoCorredor);
		
		JLabel lblPatrocinador = new JLabel(Messages.getString("InfoParticipante.lblPatrocinador")); //$NON-NLS-1$
		GridBagConstraints gbc_lblPatrocinador = new GridBagConstraints();
		gbc_lblPatrocinador.anchor = GridBagConstraints.WEST;
		gbc_lblPatrocinador.insets = new Insets(0, 0, 5, 5);
		gbc_lblPatrocinador.gridx = 1;
		gbc_lblPatrocinador.gridy = 10;
		add(lblPatrocinador, gbc_lblPatrocinador);
		
		cbPatrocinador = new JComboBox();
		cbPatrocinador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				competitor.setSponsor( (Sponsor) cbPatrocinador.getSelectedItem() );
			}
		});
		cbPatrocinador.setModel(new DefaultComboBoxModel( SponsorManager.readAllBasic() ) );
		GridBagConstraints gbc_cbPatrocinador = new GridBagConstraints();
		gbc_cbPatrocinador.gridwidth = 2;
		gbc_cbPatrocinador.insets = new Insets(0, 0, 5, 5);
		gbc_cbPatrocinador.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbPatrocinador.gridx = 2;
		gbc_cbPatrocinador.gridy = 10;
		add(cbPatrocinador, gbc_cbPatrocinador);
		
		JLabel lblClubDeportivo = new JLabel(Messages.getString("InfoParticipante.lblClubDeportivo")); //$NON-NLS-1$
		GridBagConstraints gbc_lblClubDeportivo = new GridBagConstraints();
		gbc_lblClubDeportivo.anchor = GridBagConstraints.WEST;
		gbc_lblClubDeportivo.insets = new Insets(0, 0, 5, 5);
		gbc_lblClubDeportivo.gridx = 1;
		gbc_lblClubDeportivo.gridy = 11;
		add(lblClubDeportivo, gbc_lblClubDeportivo);
		
		cbClubDeportivo = new JComboBox();
		cbClubDeportivo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				competitor.setClub( (Club) cbClubDeportivo.getSelectedItem() );
			}
		});
		cbClubDeportivo.setModel(new DefaultComboBoxModel( ClubManager.readAllBasic() ) );
		GridBagConstraints gbc_cbClubDeportivo = new GridBagConstraints();
		gbc_cbClubDeportivo.insets = new Insets(0, 0, 5, 5);
		gbc_cbClubDeportivo.gridwidth = 2;
		gbc_cbClubDeportivo.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbClubDeportivo.gridx = 2;
		gbc_cbClubDeportivo.gridy = 11;
		add(cbClubDeportivo, gbc_cbClubDeportivo);
		
		this.read( this.competitor );
	}
	
	public void read( Competitor competitor ) {
		this.tfNombre.setText( competitor.getName() );
		this. tfApellidos.setText( competitor.getSurname() );
		this.ftfDNI.setText( competitor.getDni() );
		this.ftfCorreoElectronico.setText( competitor.getEmail() );
		
		Date date;
		date = this.competitor.getBornDate();
		((MyDateModel) this.dpFechaNacimiento.getModel() ).setDate( date );
		
		this.cbLocalidad.setSelectedItem( competitor.getCity() );
		if ( !this.cbLocalidad.getSelectedItem().equals( competitor.getCity() )  )
			competitor.setCity( (String) this.cbLocalidad.getSelectedItem() );
		
		this.ftfTelefono.setText( competitor.getTelephone() );
		
		this.cbTallaCamiseta.setSelectedItem( competitor.getShirtSize() );
		if ( !this.cbTallaCamiseta.getSelectedItem().equals( competitor.getShirtSize() )  )
			competitor.setShirtSize( (String) this.cbTallaCamiseta.getSelectedItem() );
		
		this.cbTipoCorredor.setSelectedItem( competitor.getTypeRunner() );
		if ( !this.cbTipoCorredor.getSelectedItem().equals( competitor.getTypeRunner() )  )
			competitor.setTypeRunner( (String) this.cbTipoCorredor.getSelectedItem() );
		
		this.cbPatrocinador.setSelectedItem( competitor.getSponsor() );
		if ( !competitor.getSponsor().equals( this.cbPatrocinador.getSelectedItem() )  )
			competitor.setSponsor( (Sponsor) this.cbPatrocinador.getSelectedItem() );
		
		this.cbClubDeportivo.setSelectedItem( competitor.getClub() );
		if ( !competitor.getClub().equals( this.cbClubDeportivo.getSelectedItem() )  )
			competitor.setClub( (Club) this.cbClubDeportivo.getSelectedItem() );
		
//		this.cbTallaCamiseta.setSelectedItem( competitor.getShirtSize() );
//		this.cbTipoCorredor.setSelectedItem( competitor.getTypeRunner() );
//		this.cbPatrocinador.setSelectedItem( competitor.getSponsor() );
//		this.cbClubDeportivo.setSelectedItem( competitor.getClub() );
	}
}
