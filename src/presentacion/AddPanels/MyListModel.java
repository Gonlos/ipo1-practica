package presentacion.AddPanels;

import java.util.Vector;

import javax.swing.AbstractListModel;

public class MyListModel<E> extends AbstractListModel<E> {
	private Vector<E> values;
	private InterfaceUpdate update;
	
	public MyListModel( Vector<E> values ) {
		this.values = values;
		this.update = null;
	}
	
	public void setUpdate(InterfaceUpdate update) {
		this.update = update;
	}
	
	@Override
	public E getElementAt(int arg0) {
		// TODO Auto-generated method stub
		return this.values.get( arg0 );
	}

	@Override
	public int getSize() {
		return this.values.size();
	}
	
	public Vector<E> getElements() {
		// TODO Auto-generated method stub
		return this.values;
	}
	
	public boolean removeElement( Object obj ) {
		// TODO Auto-generated method stub
		int index = this.values.indexOf( obj );
		if ( index < 0 ) return false;
		
		this.removeElementAt( index );
		return true;
	}
	
	public void removeElementAt(int index) {
		// TODO Auto-generated method stub
		this.values.removeElementAt(index);
		
		this.fireIntervalRemoved( this, index, index );
		this.update();
	}

	public void insertElementAt(E data, int index) {
		// TODO Auto-generated method stub
		this.values.insertElementAt( data, index);
		
		//this.fireIntervalAdded( this, index, index );
		this.fireIntervalAdded( this, index, index );
		this.update();
	}
	
	private void update() {
		if ( this.update != null )
			this.update.update();
	}
}
