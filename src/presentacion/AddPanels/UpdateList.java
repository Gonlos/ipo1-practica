package presentacion.AddPanels;

import javax.swing.JList;

import presentacion.SelectedItems.InterfaceSelectedItems;

public class UpdateList implements InterfaceUpdate {
	private PnlSelectItems panelSI;
	private InterfaceSelectedItems si;
	private MyListModel<Object> myLM;
	
	public UpdateList(PnlSelectItems panelSI, InterfaceSelectedItems si, MyListModel<Object> myLM) {
		this.panelSI = panelSI;
		this.si = si;
		this.myLM = myLM;
	}

	public boolean update() {
		this.panelSI.updateNumElements();
		this.si.setChosenItems( myLM.getElements() );
		return true;
	}
}
