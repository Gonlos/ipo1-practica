package presentacion.AddPanels;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.DefaultFormatter;

public class RegexPatternFormatter extends DefaultFormatter {

	private Pattern pattern;
	private Matcher matcher;

	public RegexPatternFormatter( String regularExpression ) {
		setOverwriteMode(false);
		this.pattern = Pattern.compile(regularExpression);
		this.matcher = pattern.matcher(""); // create a Matcher for the regular expression //$NON-NLS-1$
	}

	public Object stringToValue(String string) throws java.text.ParseException {
		if (string == null)
			return null;
		
		matcher.reset(string); // set 'string' as the matcher's input

		if (!matcher.matches()) // Does 'string' match the regular expression?
			throw new java.text.ParseException("does not match regex", 0); //$NON-NLS-1$

		// If we get this far, then it did match.
		return super.stringToValue(string); // will honor the 'valueClass'
		// property
	}
}