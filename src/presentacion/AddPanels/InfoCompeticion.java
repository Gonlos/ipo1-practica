package presentacion.AddPanels;

import javax.swing.JPanel;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.Date;
import java.util.Properties;

import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JTextField;
import javax.swing.JEditorPane;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JTextArea;
import javax.swing.JCheckBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.MaskFormatter;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import presentacion.Messages.Messages;
import dominio.CityList;
import dominio.Competition;
import dominio.OrganizingEntity;
import dominio.OrganizingEntityManager;

public class InfoCompeticion extends JPanel {
	private JTextField tfNombre;
	private Competition competition;
	private JTextArea taDescripcion;
	private JComboBox cbLocalidad;
	private JComboBox cbModalidad;
	private JComboBox cbDificultad;
	private JFormattedTextField ftfDistanciaRecorrido;
	private JTextArea taPremios;
	private JFormattedTextField ftfPrecioInscripcion;
	private JTextArea taEnlacesInteres;
	private JTextArea taOtros;
	private JFormattedTextField ftfLimiteInscritos;
	private JCheckBox cbCarreraCompletada;
	private JComboBox cbEntidadOrganizadora;
	private JDatePickerImpl dpFechaCarrera;
	private JDatePickerImpl dpFechaLimiteInscripciones;
	

	/**
	 * Create the panel.
	 */
	public InfoCompeticion( Competition myCompetition ) {
		this.competition = myCompetition;
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{20, 0, 95, 159, 0, 20, 0};
		gridBagLayout.rowHeights = new int[]{16, 0, 72, 0, 0, 0, 0, 0, 0, 72, 0, 72, 72, 0, 0, 0, 16, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblNombre = new JLabel(Messages.getString("InfoCompeticion.lblNombre")); //$NON-NLS-1$
		GridBagConstraints gbc_lblNombre = new GridBagConstraints();
		gbc_lblNombre.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblNombre.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombre.gridx = 1;
		gbc_lblNombre.gridy = 1;
		add(lblNombre, gbc_lblNombre);
		
		tfNombre = new JTextField();
		tfNombre.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void insertUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void removeUpdate(DocumentEvent arg0) { warn(); }
			
			public void warn() {
				competition.setName( tfNombre.getText() );
			}
		});
		
		GridBagConstraints gbc_tfNombre = new GridBagConstraints();
		gbc_tfNombre.gridwidth = 2;
		gbc_tfNombre.insets = new Insets(0, 0, 5, 5);
		gbc_tfNombre.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfNombre.gridx = 2;
		gbc_tfNombre.gridy = 1;
		add(tfNombre, gbc_tfNombre);
		tfNombre.setColumns(10);
		
		JLabel lblDescripcion = new JLabel(Messages.getString("InfoCompeticion.lblDescripcion")); //$NON-NLS-1$
		GridBagConstraints gbc_lblDescripcion = new GridBagConstraints();
		gbc_lblDescripcion.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblDescripcion.insets = new Insets(0, 0, 5, 5);
		gbc_lblDescripcion.gridx = 1;
		gbc_lblDescripcion.gridy = 2;
		add(lblDescripcion, gbc_lblDescripcion);
		
		taDescripcion = new JTextArea();
		taDescripcion.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void insertUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void removeUpdate(DocumentEvent arg0) { warn(); }
			
			public void warn() {
				competition.setDescription( taDescripcion.getText() );
			}
		});
		
		GridBagConstraints gbc_taDescripcion = new GridBagConstraints();
		gbc_taDescripcion.gridwidth = 3;
		gbc_taDescripcion.insets = new Insets(0, 0, 5, 5);
		gbc_taDescripcion.fill = GridBagConstraints.BOTH;
		gbc_taDescripcion.gridx = 2;
		gbc_taDescripcion.gridy = 2;
		add(taDescripcion, gbc_taDescripcion);
		
		JLabel lblFecha = new JLabel(Messages.getString("InfoCompeticion.lblFecha")); //$NON-NLS-1$
		GridBagConstraints gbc_lblFecha = new GridBagConstraints();
		gbc_lblFecha.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblFecha.insets = new Insets(0, 0, 5, 5);
		gbc_lblFecha.gridx = 1;
		gbc_lblFecha.gridy = 3;
		add(lblFecha, gbc_lblFecha);
		
		UtilDateModel model1 = new MyDateModel();
		JDatePanelImpl datePanel1 = new JDatePanelImpl(model1, new Properties());
		dpFechaCarrera = new JDatePickerImpl(datePanel1, new DateLabelFormatter() );
		dpFechaCarrera.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Date selectedDate = (Date) dpFechaCarrera.getModel().getValue();
				competition.setDate(selectedDate);
			}
		});
		GridBagConstraints gbc_dpFechaCarrera = new GridBagConstraints();
		gbc_dpFechaCarrera.insets = new Insets(0, 0, 5, 5);
		gbc_dpFechaCarrera.gridx = 2;
		gbc_dpFechaCarrera.gridy = 3;
		add(dpFechaCarrera, gbc_dpFechaCarrera);
		
		JLabel lblFechaLimiteInscripcion = new JLabel(Messages.getString("InfoCompeticion.lblFechaLimiteInscripcion")); //$NON-NLS-1$
		GridBagConstraints gbc_lblFechaLimiteInscripcion = new GridBagConstraints();
		gbc_lblFechaLimiteInscripcion.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblFechaLimiteInscripcion.insets = new Insets(0, 0, 5, 5);
		gbc_lblFechaLimiteInscripcion.gridx = 1;
		gbc_lblFechaLimiteInscripcion.gridy = 4;
		add(lblFechaLimiteInscripcion, gbc_lblFechaLimiteInscripcion);
		
		UtilDateModel model2 = new MyDateModel();
		JDatePanelImpl datePanel2 = new JDatePanelImpl(model2, new Properties());
		dpFechaLimiteInscripciones = new JDatePickerImpl(datePanel2, new DateLabelFormatter() );
		dpFechaLimiteInscripciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Date selectedDate = (Date) dpFechaLimiteInscripciones.getModel().getValue();
				competition.setLimitInscriptionDate(selectedDate);
			}
		});
		GridBagConstraints gbc_dpFechaLimiteInscripciones = new GridBagConstraints();
		gbc_dpFechaLimiteInscripciones.insets = new Insets(0, 0, 5, 5);
		gbc_dpFechaLimiteInscripciones.gridx = 2;
		gbc_dpFechaLimiteInscripciones.gridy = 4;
		add(dpFechaLimiteInscripciones, gbc_dpFechaLimiteInscripciones);

		JLabel lblLocalidad = new JLabel(Messages.getString("InfoCompeticion.lblLocalidad")); //$NON-NLS-1$
		GridBagConstraints gbc_lblLocalidad = new GridBagConstraints();
		gbc_lblLocalidad.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblLocalidad.insets = new Insets(0, 0, 5, 5);
		gbc_lblLocalidad.gridx = 1;
		gbc_lblLocalidad.gridy = 5;
		add(lblLocalidad, gbc_lblLocalidad);
		
		cbLocalidad = new JComboBox();
		cbLocalidad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				competition.setCity( (String) cbLocalidad.getSelectedItem() );
			}
		});
		
		cbLocalidad.setModel(new DefaultComboBoxModel( CityList.CITY_LIST ) );
		GridBagConstraints gbc_cbLocalidad = new GridBagConstraints();
		gbc_cbLocalidad.gridwidth = 3;
		gbc_cbLocalidad.insets = new Insets(0, 0, 5, 5);
		gbc_cbLocalidad.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbLocalidad.gridx = 2;
		gbc_cbLocalidad.gridy = 5;
		add(cbLocalidad, gbc_cbLocalidad);
		
		JLabel lblModalidad = new JLabel(Messages.getString("InfoCompeticion.lblModalidad")); //$NON-NLS-1$
		GridBagConstraints gbc_lblModalidad = new GridBagConstraints();
		gbc_lblModalidad.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblModalidad.insets = new Insets(0, 0, 5, 5);
		gbc_lblModalidad.gridx = 1;
		gbc_lblModalidad.gridy = 6;
		add(lblModalidad, gbc_lblModalidad);
		
		cbModalidad = new JComboBox();
		cbModalidad.setModel(new DefaultComboBoxModel(new String[] {Messages.getString("InfoCompeticion.ModalidadItemCarrera"), Messages.getString("InfoCompeticion.ModalidadItemMaraton"), Messages.getString("InfoCompeticion.ModalidadItemCiclismo"), Messages.getString("InfoCompeticion.ModalidadItemMontaña")})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		cbModalidad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				competition.setModality( (String) cbModalidad.getSelectedItem() );
			}
		});
		GridBagConstraints gbc_cbModalidad = new GridBagConstraints();
		gbc_cbModalidad.gridwidth = 3;
		gbc_cbModalidad.insets = new Insets(0, 0, 5, 5);
		gbc_cbModalidad.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbModalidad.gridx = 2;
		gbc_cbModalidad.gridy = 6;
		add(cbModalidad, gbc_cbModalidad);
		
		JLabel lblDificultad = new JLabel(Messages.getString("InfoCompeticion.lblDificultad")); //$NON-NLS-1$
		GridBagConstraints gbc_lblDificultad = new GridBagConstraints();
		gbc_lblDificultad.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblDificultad.insets = new Insets(0, 0, 5, 5);
		gbc_lblDificultad.gridx = 1;
		gbc_lblDificultad.gridy = 7;
		add(lblDificultad, gbc_lblDificultad);
		
		cbDificultad = new JComboBox();
		cbDificultad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				competition.setLevel( (String) cbDificultad.getSelectedItem() );
			}
		});
		cbDificultad.setModel(new DefaultComboBoxModel(new String[] {Messages.getString("InfoCompeticion.DificultadItemAlta"), Messages.getString("InfoCompeticion.DificultadItemMedia"), Messages.getString("InfoCompeticion.DificultadItemBaja")})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		GridBagConstraints gbc_cbDificultad = new GridBagConstraints();
		gbc_cbDificultad.gridwidth = 3;
		gbc_cbDificultad.insets = new Insets(0, 0, 5, 5);
		gbc_cbDificultad.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbDificultad.gridx = 2;
		gbc_cbDificultad.gridy = 7;
		add(cbDificultad, gbc_cbDificultad);
		
		JLabel lblDistanciaRecorrido = new JLabel(Messages.getString("InfoCompeticion.lblDistanciaRecorrido")); //$NON-NLS-1$
		GridBagConstraints gbc_lblDistanciaRecorrido = new GridBagConstraints();
		gbc_lblDistanciaRecorrido.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblDistanciaRecorrido.insets = new Insets(0, 0, 5, 5);
		gbc_lblDistanciaRecorrido.gridx = 1;
		gbc_lblDistanciaRecorrido.gridy = 8;
		add(lblDistanciaRecorrido, gbc_lblDistanciaRecorrido);
		
		ftfDistanciaRecorrido = new JFormattedTextField( new RegexPatternFormatter( "^(?:\\+|-)?\\d+$" ) ); //$NON-NLS-1$
		ftfDistanciaRecorrido.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void insertUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void removeUpdate(DocumentEvent arg0) { warn(); }
			
			public void warn() {
				try {
					competition.setDistance( Integer.parseInt( ftfDistanciaRecorrido.getText() ) );
				} catch ( Exception e ) {
				}
			}
		});
		
		GridBagConstraints gbc_ftfDistanciaRecorrido = new GridBagConstraints();
		gbc_ftfDistanciaRecorrido.insets = new Insets(0, 0, 5, 5);
		gbc_ftfDistanciaRecorrido.fill = GridBagConstraints.HORIZONTAL;
		gbc_ftfDistanciaRecorrido.gridx = 2;
		gbc_ftfDistanciaRecorrido.gridy = 8;
		add(ftfDistanciaRecorrido, gbc_ftfDistanciaRecorrido);
		
		JLabel lblMetros = new JLabel(Messages.getString("InfoCompeticion.lblMetros")); //$NON-NLS-1$
		GridBagConstraints gbc_lblMetros = new GridBagConstraints();
		gbc_lblMetros.anchor = GridBagConstraints.WEST;
		gbc_lblMetros.insets = new Insets(0, 0, 5, 5);
		gbc_lblMetros.gridx = 3;
		gbc_lblMetros.gridy = 8;
		add(lblMetros, gbc_lblMetros);
		
		JLabel lblPremios = new JLabel(Messages.getString("InfoCompeticion.lblPremios")); //$NON-NLS-1$
		GridBagConstraints gbc_lblPremios = new GridBagConstraints();
		gbc_lblPremios.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblPremios.insets = new Insets(0, 0, 5, 5);
		gbc_lblPremios.gridx = 1;
		gbc_lblPremios.gridy = 9;
		add(lblPremios, gbc_lblPremios);
		
		taPremios = new JTextArea();
		taPremios.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void insertUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void removeUpdate(DocumentEvent arg0) { warn(); }
			
			public void warn() {
				competition.setAward( taPremios.getText() );
			}
		});
		
		GridBagConstraints gbc_taPremios = new GridBagConstraints();
		gbc_taPremios.gridwidth = 3;
		gbc_taPremios.insets = new Insets(0, 0, 5, 5);
		gbc_taPremios.fill = GridBagConstraints.BOTH;
		gbc_taPremios.gridx = 2;
		gbc_taPremios.gridy = 9;
		add(taPremios, gbc_taPremios);
		
		JLabel lblPrecioInscripcion = new JLabel(Messages.getString("InfoCompeticion.lblPrecioInscripcion")); //$NON-NLS-1$
		GridBagConstraints gbc_lblPrecioInscripcion = new GridBagConstraints();
		gbc_lblPrecioInscripcion.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblPrecioInscripcion.insets = new Insets(0, 0, 5, 5);
		gbc_lblPrecioInscripcion.gridx = 1;
		gbc_lblPrecioInscripcion.gridy = 10;
		add(lblPrecioInscripcion, gbc_lblPrecioInscripcion);

		ftfPrecioInscripcion = new JFormattedTextField( new RegexPatternFormatter( "\\d*(.\\d{1,3})?" ) );		 //$NON-NLS-1$
		ftfPrecioInscripcion.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void insertUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void removeUpdate(DocumentEvent arg0) { warn(); }
			
			public void warn() {
				try {
					competition.setFee( Float.parseFloat( ftfPrecioInscripcion.getText() ) );
				} catch ( Exception e ) {
				}
			}
		});
		
		GridBagConstraints gbc_ftfPrecioInscripcion = new GridBagConstraints();
		gbc_ftfPrecioInscripcion.insets = new Insets(0, 0, 5, 5);
		gbc_ftfPrecioInscripcion.fill = GridBagConstraints.HORIZONTAL;
		gbc_ftfPrecioInscripcion.gridx = 2;
		gbc_ftfPrecioInscripcion.gridy = 10;
		add(ftfPrecioInscripcion, gbc_ftfPrecioInscripcion);
		
		JLabel label = new JLabel("€"); //$NON-NLS-1$
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.anchor = GridBagConstraints.WEST;
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 3;
		gbc_label.gridy = 10;
		add(label, gbc_label);
		
		JLabel lblEnlacesInteres = new JLabel(Messages.getString("InfoCompeticion.lblEnlacesInteres")); //$NON-NLS-1$
		GridBagConstraints gbc_lblEnlacesInteres = new GridBagConstraints();
		gbc_lblEnlacesInteres.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblEnlacesInteres.insets = new Insets(0, 0, 5, 5);
		gbc_lblEnlacesInteres.gridx = 1;
		gbc_lblEnlacesInteres.gridy = 11;
		add(lblEnlacesInteres, gbc_lblEnlacesInteres);
		
		taEnlacesInteres = new JTextArea();
		taEnlacesInteres.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void insertUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void removeUpdate(DocumentEvent arg0) { warn(); }
			
			public void warn() {
				competition.setWebLinks( taEnlacesInteres.getText() );
			}
		});
		
		GridBagConstraints gbc_taEnlacesInteres = new GridBagConstraints();
		gbc_taEnlacesInteres.gridwidth = 3;
		gbc_taEnlacesInteres.insets = new Insets(0, 0, 5, 5);
		gbc_taEnlacesInteres.fill = GridBagConstraints.BOTH;
		gbc_taEnlacesInteres.gridx = 2;
		gbc_taEnlacesInteres.gridy = 11;
		add(taEnlacesInteres, gbc_taEnlacesInteres);
		
		JLabel lblOtros = new JLabel(Messages.getString("InfoCompeticion.lblOtros")); //$NON-NLS-1$
		GridBagConstraints gbc_lblOtros = new GridBagConstraints();
		gbc_lblOtros.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblOtros.insets = new Insets(0, 0, 5, 5);
		gbc_lblOtros.gridx = 1;
		gbc_lblOtros.gridy = 12;
		add(lblOtros, gbc_lblOtros);
		
		taOtros = new JTextArea();
		taOtros.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void insertUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void removeUpdate(DocumentEvent arg0) { warn(); }
			
			public void warn() {
				competition.setOtherInfo( taOtros.getText() );
			}
		});
		
		GridBagConstraints gbc_taOtros = new GridBagConstraints();
		gbc_taOtros.gridwidth = 3;
		gbc_taOtros.insets = new Insets(0, 0, 5, 5);
		gbc_taOtros.fill = GridBagConstraints.BOTH;
		gbc_taOtros.gridx = 2;
		gbc_taOtros.gridy = 12;
		add(taOtros, gbc_taOtros);
		
		JLabel lblLmiteDeInscritos = new JLabel(Messages.getString("InfoCompeticion.lblLmiteDeInscritos")); //$NON-NLS-1$
		GridBagConstraints gbc_lblLmiteDeInscritos = new GridBagConstraints();
		gbc_lblLmiteDeInscritos.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblLmiteDeInscritos.insets = new Insets(0, 0, 5, 5);
		gbc_lblLmiteDeInscritos.gridx = 1;
		gbc_lblLmiteDeInscritos.gridy = 13;
		add(lblLmiteDeInscritos, gbc_lblLmiteDeInscritos);
		
		ftfLimiteInscritos = new JFormattedTextField( new RegexPatternFormatter( "^(?:\\+|-)?\\d+$") ); //$NON-NLS-1$
		ftfLimiteInscritos.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void insertUpdate(DocumentEvent arg0) { warn(); }
			
			@Override
			public void removeUpdate(DocumentEvent arg0) { warn(); }
			
			public void warn() {
				try {
					competition.setCompetitorsLimit( Integer.parseInt( ftfLimiteInscritos.getText() ) );
				} catch ( Exception e ) {
				}
			}
		});
		
		GridBagConstraints gbc_ftfLimiteInscritos = new GridBagConstraints();
		gbc_ftfLimiteInscritos.insets = new Insets(0, 0, 5, 5);
		gbc_ftfLimiteInscritos.fill = GridBagConstraints.HORIZONTAL;
		gbc_ftfLimiteInscritos.gridx = 2;
		gbc_ftfLimiteInscritos.gridy = 13;
		add(ftfLimiteInscritos, gbc_ftfLimiteInscritos);
		
		JLabel lblCarreraCompletada = new JLabel(Messages.getString("InfoCompeticion.lblCarreraCompletada")); //$NON-NLS-1$
		GridBagConstraints gbc_lblCarreraCompletada = new GridBagConstraints();
		gbc_lblCarreraCompletada.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblCarreraCompletada.insets = new Insets(0, 0, 5, 5);
		gbc_lblCarreraCompletada.gridx = 1;
		gbc_lblCarreraCompletada.gridy = 14;
		add(lblCarreraCompletada, gbc_lblCarreraCompletada);
		
		cbCarreraCompletada = new JCheckBox(""); //$NON-NLS-1$
		cbCarreraCompletada.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				competition.setComplete( cbCarreraCompletada.isSelected() );
			}
		});
		
		GridBagConstraints gbc_cbCarreraCompletada = new GridBagConstraints();
		gbc_cbCarreraCompletada.gridwidth = 2;
		gbc_cbCarreraCompletada.insets = new Insets(0, 0, 5, 5);
		gbc_cbCarreraCompletada.anchor = GridBagConstraints.WEST;
		gbc_cbCarreraCompletada.gridx = 2;
		gbc_cbCarreraCompletada.gridy = 14;
		add(cbCarreraCompletada, gbc_cbCarreraCompletada);
		
		JLabel lblEntidadOrganizadora = new JLabel(Messages.getString("InfoCompeticion.lblEntidadOrganizadora")); //$NON-NLS-1$
		GridBagConstraints gbc_lblEntidadOrganizadora = new GridBagConstraints();
		gbc_lblEntidadOrganizadora.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblEntidadOrganizadora.insets = new Insets(0, 0, 5, 5);
		gbc_lblEntidadOrganizadora.gridx = 1;
		gbc_lblEntidadOrganizadora.gridy = 15;
		add(lblEntidadOrganizadora, gbc_lblEntidadOrganizadora);
		
		cbEntidadOrganizadora = new JComboBox( new DefaultComboBoxModel( OrganizingEntityManager.readAllBasic() )  );
		cbEntidadOrganizadora.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				competition.setOrganizingEntity( (OrganizingEntity) cbEntidadOrganizadora.getSelectedItem() );
			}
		});
		
		GridBagConstraints gbc_cbEntidadOrganizadora = new GridBagConstraints();
		gbc_cbEntidadOrganizadora.gridwidth = 3;
		gbc_cbEntidadOrganizadora.insets = new Insets(0, 0, 5, 5);
		gbc_cbEntidadOrganizadora.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbEntidadOrganizadora.gridx = 2;
		gbc_cbEntidadOrganizadora.gridy = 15;
		add(cbEntidadOrganizadora, gbc_cbEntidadOrganizadora);
		
		this.read( this.competition );
	}
	
	public void read( Competition competition ) {
		this.tfNombre.setText( competition.getName() );
		this.taDescripcion.setText( competition.getDescription() );
		this.ftfDistanciaRecorrido.setText( String.valueOf( competition.getDistance() ) );
		this.taPremios.setText( competition.getAward() );
		this.ftfPrecioInscripcion.setText( String.valueOf( competition.getFee() ) );
		this.taEnlacesInteres.setText( competition.getWebLinks() );
		this.taOtros.setText( competition.getOtherInfo() );
		this.ftfLimiteInscritos.setText( String.valueOf( competition.getCompetitorsLimit() ) );
		this.cbCarreraCompletada.setSelected( competition.isComplete() );
		
		Date date;
		date = this.competition.getDate();
		((MyDateModel) this.dpFechaCarrera.getModel() ).setDate( date );
		
		date = this.competition.getLimitInscriptionDate();
		((MyDateModel) this.dpFechaLimiteInscripciones.getModel() ).setDate( date );
		
		this.cbLocalidad.setSelectedItem( competition.getCity() );
		if ( !this.cbLocalidad.getSelectedItem().equals( competition.getCity() )  )
			competition.setCity( (String) this.cbLocalidad.getSelectedItem() );
		
		this.cbModalidad.setSelectedItem( competition.getModality() );
		if ( !this.cbModalidad.getSelectedItem().equals( competition.getModality() )  )
			competition.setModality( (String) this.cbModalidad.getSelectedItem() );
		
		this.cbDificultad.setSelectedItem( competition.getLevel() );
		if ( !this.cbDificultad.getSelectedItem().equals( competition.getLevel() )  )
			competition.setLevel( (String) this.cbDificultad.getSelectedItem() );
		
		this.cbEntidadOrganizadora.setSelectedItem( competition.getOrganizingEntity() );
		if ( !competition.getOrganizingEntity().equals( this.cbEntidadOrganizadora.getSelectedItem() )  )
			competition.setOrganizingEntity( (OrganizingEntity) this.cbEntidadOrganizadora.getSelectedItem() );
	}

}
