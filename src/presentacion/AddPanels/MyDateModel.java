package presentacion.AddPanels;

import java.util.Date;

import org.jdatepicker.impl.UtilDateModel;

public class MyDateModel extends UtilDateModel {
	public void setDate( Date date ) {
		this.setDate(1900 + date.getYear(), date.getMonth(), date.getDay());
		this.setSelected(true);
	}
}
