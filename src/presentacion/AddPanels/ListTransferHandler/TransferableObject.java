package presentacion.AddPanels.ListTransferHandler;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;

public class TransferableObject implements Transferable {
	protected static DataFlavor objectFlavor = new DataFlavor(Object.class, "A generic Object");
	protected static DataFlavor[] supportedFlavors = { objectFlavor };
	Object object;

	public static DataFlavor getObjectFlavor() {
		return objectFlavor;
	}
	
	public TransferableObject(Object object) {
		this.object = object;
	}

	public DataFlavor[] getTransferDataFlavors() {
		return supportedFlavors;
	}

	public boolean isDataFlavorSupported(DataFlavor flavor) {
		if (flavor.equals(object) || flavor.equals(DataFlavor.stringFlavor))
			return true;
		return false;
	}

	public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException {
		if (flavor.equals(objectFlavor))
			return object;
		else if (flavor.equals(DataFlavor.stringFlavor))
			return object.toString();
		else
			throw new UnsupportedFlavorException(flavor);
	}
}