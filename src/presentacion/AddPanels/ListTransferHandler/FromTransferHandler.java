package presentacion.AddPanels.ListTransferHandler;

import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.TransferHandler;

import presentacion.AddPanels.MyListModel;

public class FromTransferHandler extends TransferHandler {
	private MyListModel from;
	private JList dragFrom;
	
	public FromTransferHandler( MyListModel from, JList dragFrom ) {
		this.from = from;
		this.dragFrom = dragFrom;
	}
	
    public int getSourceActions(JComponent comp) {
        return COPY_OR_MOVE;
    }

    private int index = 0;

    public Transferable createTransferable(JComponent comp) {
        index = dragFrom.getSelectedIndex();
        if (index < 0 || index >= from.getSize()) {
            return null;
        }

        return new TransferableObject( dragFrom.getSelectedValue() );
    }
    
    public void exportDone(JComponent comp, Transferable trans, int action) {
        if (action != MOVE) {
            return;
        }

        this.from.removeElementAt(index);
    }
}
