package presentacion.AddPanels.ListTransferHandler;

import java.awt.Rectangle;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.TransferHandler;

import presentacion.AddPanels.MyListModel;

public class BiTransferHandler extends TransferHandler {
	private MyListModel from;
	private JList dragFrom;
	
	private int indexDrag = 0;
	private int indexDrop = 0;
	
	private int count = 0;
	private int dragCount;
	
    int action;
	
	public BiTransferHandler( MyListModel from, JList dragFrom, int action ) {
		this.from = from;
		this.dragFrom = dragFrom;
		this.action = action;
	}
	
    public int getSourceActions(JComponent comp) {
        return COPY_OR_MOVE;
    }

    public Transferable createTransferable(JComponent comp) {
        this.indexDrag = dragFrom.getSelectedIndex();
        if (this.indexDrag < 0 || this.indexDrag >= this.from.getSize()) {
            return null;
        }
        
        this.count++;
        this.dragCount = this.count;

        return new TransferableObject( this.dragFrom.getSelectedValue() );
    }
    
    public void exportDone(JComponent comp, Transferable trans, int action) {
        if (action != MOVE) {
            return;
        }

        int index = this.indexDrag;
        
        boolean moveToMe = this.count != this.dragCount;
        if ( moveToMe && ( this.indexDrop < this.indexDrag ) ) index++;
        
        this.from.removeElementAt(index);
    }
    
    public boolean canImport(TransferHandler.TransferSupport support) {
        // for the demo, we'll only support drops (not clipboard paste)
        if (!support.isDrop()) {
            return false;
        }

        // we only import Strings
        if (!support.isDataFlavorSupported( TransferableObject.getObjectFlavor() )) {
            return false;
        }

        boolean actionSupported = (action & support.getSourceDropActions()) == action;
        if (actionSupported) {
            support.setDropAction(action);
            return true;
        }

        return false;
    }

    public boolean importData(TransferHandler.TransferSupport support) {
        // if we can't handle the import, say so
        if (!canImport(support)) {
            return false;
        }

        // fetch the drop location
        JList.DropLocation dl = (JList.DropLocation)support.getDropLocation();

        this.indexDrop = dl.getIndex();

        // fetch the data and bail if this fails
        Object data;
        try {
            data = support.getTransferable().getTransferData( TransferableObject.getObjectFlavor() );
        } catch (UnsupportedFlavorException e) {
            return false;
        } catch (java.io.IOException e) {
            return false;
        }
        
        this.count++;

        JList list = (JList)support.getComponent();
        MyListModel model = (MyListModel) list.getModel();
        model.insertElementAt(data, this.indexDrop);

        Rectangle rect = list.getCellBounds(this.indexDrop, this.indexDrop);
        list.scrollRectToVisible(rect);
        list.setSelectedIndex(this.indexDrop);
        list.requestFocusInWindow();

        return true;
    }  
}
