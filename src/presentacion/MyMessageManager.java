package presentacion;

import java.awt.Component;

import javax.swing.JOptionPane;

import persistencia.AgentMySQL;
import presentacion.Messages.Messages;

public class MyMessageManager {
	private Component parentComponent;
	static private MyMessageManager mInstancia;

	private MyMessageManager() {
	}
	
	public static MyMessageManager getMessageManager() {
		try {
			if (mInstancia == null) {
				mInstancia = new MyMessageManager();
			}

			return mInstancia;
		} catch (Exception e) {
		}

		return null;
	}
	
	public boolean showConfirmDeleteMessage( String title ) {
		Object[] botonesOptionPane = { Messages.getString("MyMessageManager.OpcionSi"), Messages.getString("MyMessageManager.OpcionNo") };
		
		int dialogResult = JOptionPane.showOptionDialog( this.parentComponent, String.format( Messages.getString("MyMessageManager.TextDeleteMessage"), title ), //$NON-NLS-1$
				Messages.getString("MyMessageManager.TitleDeleteMessage"),	JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, botonesOptionPane, 1 ); //$NON-NLS-1$
		
		return dialogResult == 0;
	}
	
	public boolean showConfirmDontSave() {
		Object[] botonesOptionPane = { Messages.getString("MyMessageManager.OpcionSi"), Messages.getString("MyMessageManager.OpcionNo") };
		
		int dialogResult = JOptionPane.showOptionDialog( this.parentComponent, Messages.getString("MyMessageManager.TextChangesDontSave"), //$NON-NLS-1$
				Messages.getString("MyMessageManager.TitleChangesDontSave"),	JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, botonesOptionPane, 1 ); //$NON-NLS-1$
		
		return dialogResult == 0;
	}
	
	public void showErrorSave() {
		Object[] botonesOptionPane = { Messages.getString("MyMessageManager.OpcionAceptar") };
		
		JOptionPane.showOptionDialog( this.parentComponent, Messages.getString("MyMessageManager.TextErrorSave"), //$NON-NLS-1$
				Messages.getString("MyMessageManager.TitleErrorSave"),	JOptionPane.PLAIN_MESSAGE, JOptionPane.ERROR_MESSAGE, null, botonesOptionPane, 0 ); //$NON-NLS-1$
	}
	
	public void showWorkingMessage() {
		Object[] botonesOptionPane = { Messages.getString("MyMessageManager.OpcionAceptar") };
		
		JOptionPane.showOptionDialog( this.parentComponent, Messages.getString("MyMessageManager.TextWorking"), //$NON-NLS-1$
				Messages.getString("MyMessageManager.TitleWorking"),	JOptionPane.PLAIN_MESSAGE, JOptionPane.WARNING_MESSAGE, null, botonesOptionPane, 0 ); //$NON-NLS-1$
	}
	
	public void showUserAndPassNeed() {
		Object[] botonesOptionPane = { Messages.getString("MyMessageManager.OpcionAceptar") };
		
		JOptionPane.showOptionDialog( this.parentComponent, Messages.getString("MyMessageManager.TextUserAndPassNeed"), //$NON-NLS-1$
				Messages.getString("MyMessageManager.TitleUserAndPassNeed"),	JOptionPane.PLAIN_MESSAGE, JOptionPane.WARNING_MESSAGE, null, botonesOptionPane, 0 ); //$NON-NLS-1$
	}

	public Component getParentComponent() {
		return parentComponent;
	}

	public void setParentComponent(Component parentComponent) {
		this.parentComponent = parentComponent;
	}
	
}
