package presentacion.SelectedItems;

import java.util.Vector;

public interface InterfaceSelectedItems {
	public boolean isLimitedItems();
	public int getMaxNumItems();
	public String getTitleNoChosenItems();
	public String getTitleChosenItems();
	public Vector<Object> getNoChosenItems();
	public Vector<Object> getChosenItems();
	public void setChosenItems( Vector<Object> items );
}