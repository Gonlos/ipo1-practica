package presentacion.SelectedItems;

import java.util.Vector;

import presentacion.Messages.Messages;
import dominio.Competition;
import dominio.Competitor;
import dominio.CompetitorManager;

public class SelectedItemsCompetitorsInCompetition implements InterfaceSelectedItems {
	private Competition competition;
	
	public SelectedItemsCompetitorsInCompetition(Competition competition) {
		this.competition = competition;
	}
	
	public boolean isLimitedItems() {
		return this.competition.getCompetitorsLimit() >= 0;
	}
	
	public int getMaxNumItems() {
		return this.competition.getCompetitorsLimit();
	}
	
	public String getTitleNoChosenItems() {
		return Messages.getString("SelectedItemsCompetitorsInCompetition.TitleNoChosenItem"); //$NON-NLS-1$
	}
	
	public String getTitleChosenItems() {
		return Messages.getString("SelectedItemsCompetitorsInCompetition.TitleChosenItem"); //$NON-NLS-1$
	}
	
	public Vector<Object> getNoChosenItems() {
		Vector<Competitor> chosen = this.competition.getCompetitors();
		Vector<Competitor> v = CompetitorManager.readAllBasic();
		
		Vector<Object> result = new Vector<Object>();
		for ( Competitor c : v ) {
			if ( !chosen.contains( c ) ) {
				result.add( c );
			}
		}
		
		return result;
	}
	
	public Vector<Object> getChosenItems() {
		Vector<Competitor> v = this.competition.getCompetitors();
		
		Vector<Object> result = new Vector<Object>();
		for ( Competitor c : v ) {
			result.add( c );
		}
		
		return result;
	}
	
	public void setChosenItems( Vector<Object> items ) {
		Vector<Competitor> result = new Vector<Competitor>();
		for ( Object o : items ) {
			result.add( (Competitor) o );
		}
		
		this.competition.setCompetitors(result);
	}
}
