package presentacion.SelectedItems;

import java.util.Vector;

import presentacion.Messages.Messages;
import dominio.CollaboratingEntity;
import dominio.CollaboratingEntityManager;
import dominio.Competition;

public class SelectedItemsCollaboratingEntitiesInCompetition implements InterfaceSelectedItems {
	private Competition competition;
	
	public SelectedItemsCollaboratingEntitiesInCompetition(Competition competition) {
		this.competition = competition;
	}
	
	public boolean isLimitedItems() {
		return false;
	}
	
	public int getMaxNumItems() {
		return Integer.MAX_VALUE;
	}
	
	public String getTitleNoChosenItems() {
		return Messages.getString("SelectedItemsCollaboratingEntitiesInCompetition.TitleNoChosenItem"); //$NON-NLS-1$
	}
	
	public String getTitleChosenItems() {
		return Messages.getString("SelectedItemsCollaboratingEntitiesInCompetition.TitleChosenItem"); //$NON-NLS-1$
	}
	
	public Vector<Object> getNoChosenItems() {
		Vector<CollaboratingEntity> chosen = this.competition.getCollaboratingEntities();
		Vector<CollaboratingEntity> v = CollaboratingEntityManager.readAllBasic();
		
		Vector<Object> result = new Vector<Object>();
		for ( CollaboratingEntity c : v ) {
			if ( !chosen.contains( c ) ) {
				result.add( c );
			}
		}
		
		return result;
	}
	
	public Vector<Object> getChosenItems() {
		Vector<CollaboratingEntity> v = this.competition.getCollaboratingEntities();
		
		Vector<Object> result = new Vector<Object>();
		for ( CollaboratingEntity c : v ) {
			result.add( c );
		}
		
		return result;
	}
	
	public void setChosenItems( Vector<Object> items ) {
		Vector<CollaboratingEntity> result = new Vector<CollaboratingEntity>();
		for ( Object o : items ) {
			result.add( (CollaboratingEntity) o );
		}
		
		this.competition.setCollaboratingEntities(result);
	}
}
