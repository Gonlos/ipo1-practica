package presentacion;

import javax.swing.JPanel;

import java.awt.GridBagLayout;

import javax.swing.JButton;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.DefaultListModel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.border.MatteBorder;

import presentacion.AddPanels.InterfaceUpdate;
import presentacion.Connectors.InterfaceConnectorPnlAdd;
import presentacion.ListElements.ListAddElements;
import presentacion.Messages.Messages;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Component;

import javax.swing.ImageIcon;

public class PnlAdd extends JPanel {
	private InterfaceConnectorPnlAdd connector;
	
	JScrollPane scrollPane;
	PnlImagePreview imgPanel; 
	
	/**
	 * Create the panel.
	 */
	public PnlAdd( InterfaceAppBody panelParent, InterfaceConnectorPnlAdd myConnector ) {
		this.connector = myConnector;
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] {10, 343, -18, 249, 0};
		gridBagLayout.rowHeights = new int[]{7, 0, 7, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JPanel panel = new JPanel();
		panel.setBorder(new MatteBorder(0, 1, 0, 0, (Color) new Color(0, 0, 0)));
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridheight = 5;
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 3;
		gbc_panel.gridy = 0;
		add(panel, gbc_panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		imgPanel = new PnlImagePreview();
		imgPanel.setEditable( false );
		panel.add(imgPanel);
		
		JButton btnAdd = new JButton(Messages.getString("PnlAdd.btnAdd")); //$NON-NLS-1$
		btnAdd.setIcon(null);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				connector.addItem();
			}
		});
		btnAdd.setPreferredSize(new Dimension(140, 25));
		btnAdd.setMaximumSize(new Dimension(9999999, 25));
		GridBagConstraints gbc_btnAdd = new GridBagConstraints();
		gbc_btnAdd.anchor = GridBagConstraints.WEST;
		gbc_btnAdd.insets = new Insets(0, 0, 5, 5);
		gbc_btnAdd.gridx = 1;
		gbc_btnAdd.gridy = 1;
		add(btnAdd, gbc_btnAdd);
		
		scrollPane = new JScrollPane();
		scrollPane.setAlignmentY(Component.TOP_ALIGNMENT);
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 1;
		gbc_scrollPane.gridy = 3;
		add(scrollPane, gbc_scrollPane);
		
		this.connector.setUpdateImage( this.imgPanel );

		ListAddElements listItems = new ListAddElements();
		listItems.addItems( this.connector.getItems( listItems ) );
		this.scrollPane.setViewportView( listItems );
		
	}
}
