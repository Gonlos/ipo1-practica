package presentacion;

import javax.swing.JPanel;
import javax.swing.JMenuItem;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.border.LineBorder;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Dimension;

import javax.swing.JMenuBar;

import java.awt.FlowLayout;

import javax.swing.ImageIcon;

import presentacion.Connectors.ConnectorPnlAddClubs;
import presentacion.Connectors.ConnectorPnlAddCollaboratingEntity;
import presentacion.Connectors.ConnectorPnlAddCompetition;
import presentacion.Connectors.ConnectorPnlAddCompetitor;
import presentacion.Connectors.ConnectorPnlAddOrganizingEntity;
import presentacion.Connectors.ConnectorPnlAddSponsor;
import presentacion.Connectors.InterfaceConnectorPnlAdd;
import presentacion.Messages.Messages;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PnlAppBody extends JPanel implements InterfaceAppBody {
	private JPanel panelBody;
	private InterfaceAppBody me;

	/**
	 * Create the panel.
	 */
	public PnlAppBody() {
		this.me = this;
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{20, 0, 20, 0};
		gridBagLayout.rowHeights = new int[]{13, 67, 37, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JPanel panelHead = new JPanel();
		panelHead.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		GridBagConstraints gbc_panelHead = new GridBagConstraints();
		gbc_panelHead.insets = new Insets(0, 0, 5, 5);
		gbc_panelHead.fill = GridBagConstraints.BOTH;
		gbc_panelHead.gridx = 1;
		gbc_panelHead.gridy = 1;
		add(panelHead, gbc_panelHead);
		panelHead.setLayout(new GridLayout(5, 0, 0, 0));
		
		JPanel panelFlags = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panelFlags.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		panelHead.add(panelFlags);
		
		JLabel imgESP = new JLabel(""); //$NON-NLS-1$
		imgESP.setVisible(false);
		imgESP.setEnabled(false);
		imgESP.setToolTipText("Español"); //$NON-NLS-1$
		imgESP.setIcon(new ImageIcon(PnlAppBody.class.getResource("/presentacion/images/flags/Spain.png"))); //$NON-NLS-1$
		panelFlags.add(imgESP);
		
		JLabel imgENG = new JLabel(""); //$NON-NLS-1$
		imgENG.setEnabled(false);
		imgENG.setVisible(false);
		imgENG.setToolTipText("Ingles"); //$NON-NLS-1$
		imgENG.setIcon(new ImageIcon(PnlAppBody.class.getResource("/presentacion/images/flags/United_Kingdom.png"))); //$NON-NLS-1$
		panelFlags.add(imgENG);
		
		JPanel panel_1 = new JPanel();
		panel_1.setMinimumSize(new Dimension(10, 5));
		panel_1.setPreferredSize(new Dimension(10, 5));
		panelHead.add(panel_1);
		
		JLabel lblHolaUser = new JLabel( String.format( Messages.getString("PnlAppBody.lblHolaUser"), "Pedro" ) ); //$NON-NLS-1$
		lblHolaUser.setFont(new Font("Dialog", Font.BOLD, 11)); //$NON-NLS-1$
		lblHolaUser.setPreferredSize(new Dimension(78, 7));
		lblHolaUser.setMinimumSize(new Dimension(78, 7));
		lblHolaUser.setHorizontalAlignment(SwingConstants.RIGHT);
		panelHead.add(lblHolaUser);
		
		JLabel lblCloseSesion = new JLabel(Messages.getString("PnlAppBody.lblCloseSesion"));
		lblCloseSesion.setVisible(false);
		lblCloseSesion.setEnabled(false);
		lblCloseSesion.setFont(new Font("Dialog", Font.BOLD, 11)); //$NON-NLS-1$
		lblCloseSesion.setPreferredSize(new Dimension(96, 7));
		lblCloseSesion.setMinimumSize(new Dimension(96, 7));
		lblCloseSesion.setHorizontalAlignment(SwingConstants.RIGHT);
		panelHead.add(lblCloseSesion);
		
		JLabel lblLastAccess = new JLabel( String.format( Messages.getString("PnlAppBody.lblLastAccess"), "1/1/2014" ) ); //$NON-NLS-1$
		lblLastAccess.setFont(new Font("Dialog", Font.BOLD, 11)); //$NON-NLS-1$
		lblLastAccess.setPreferredSize(new Dimension(233, 7));
		lblLastAccess.setMinimumSize(new Dimension(233, 7));
		lblLastAccess.setHorizontalAlignment(SwingConstants.RIGHT);
		panelHead.add(lblLastAccess);
		
		JPanel panelOptions = new JPanel();
		panelOptions.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		GridBagConstraints gbc_panelOptions = new GridBagConstraints();
		gbc_panelOptions.insets = new Insets(0, 0, 5, 5);
		gbc_panelOptions.fill = GridBagConstraints.BOTH;
		gbc_panelOptions.gridx = 1;
		gbc_panelOptions.gridy = 2;
		add(panelOptions, gbc_panelOptions);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setFont(new Font("Dialog", Font.BOLD, 12)); //$NON-NLS-1$
		menuBar.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelOptions.add(menuBar);
		
		JMenuItem mntmParticipantes = new JMenuItem(Messages.getString("PnlAppBody.mntmParticipantes")); //$NON-NLS-1$
		mntmParticipantes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if ( !MyChangePanelManager.getChangePanelManager().setCurrentState( MyChangePanelManager.STATES.FREE ) ) return;
				
				InterfaceConnectorPnlAdd connector = new ConnectorPnlAddCompetitor( me );
				PnlAdd myAddPanel = new PnlAdd( me, connector );
				
				me.setMainPanel( myAddPanel );
			}
		});
		mntmParticipantes.setFont(new Font("Dialog", Font.BOLD, 11)); //$NON-NLS-1$
		menuBar.add(mntmParticipantes);
		
		JMenuItem mntmCompeticiones = new JMenuItem(Messages.getString("PnlAppBody.mntmCompeticiones")); //$NON-NLS-1$
		mntmCompeticiones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if ( !MyChangePanelManager.getChangePanelManager().setCurrentState( MyChangePanelManager.STATES.FREE ) ) return;
				
				InterfaceConnectorPnlAdd connector = new ConnectorPnlAddCompetition( me );
				PnlAdd myAddPanel = new PnlAdd( me, connector );
				
				me.setMainPanel( myAddPanel );
			}
		});
		mntmCompeticiones.setFont(new Font("Dialog", Font.BOLD, 11)); //$NON-NLS-1$
		menuBar.add(mntmCompeticiones);
		
		JMenuItem mntmEntidadesOrganizadoras = new JMenuItem(Messages.getString("PnlAppBody.mntmEntidadesOrganizadoras")); //$NON-NLS-1$
		mntmEntidadesOrganizadoras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if ( !MyChangePanelManager.getChangePanelManager().setCurrentState( MyChangePanelManager.STATES.FREE ) ) return;
				
				InterfaceConnectorPnlAdd connector = new ConnectorPnlAddOrganizingEntity( me );
				PnlAdd myAddPanel = new PnlAdd( me, connector );
				
				me.setMainPanel( myAddPanel );
			}
		});
		mntmEntidadesOrganizadoras.setFont(new Font("Dialog", Font.BOLD, 11)); //$NON-NLS-1$
		menuBar.add(mntmEntidadesOrganizadoras);
		
		JMenuItem mntmEntidadesColaboradoras = new JMenuItem(Messages.getString("PnlAppBody.mntmEntidadesColaboradoras")); //$NON-NLS-1$
		mntmEntidadesColaboradoras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if ( !MyChangePanelManager.getChangePanelManager().setCurrentState( MyChangePanelManager.STATES.FREE ) ) return;
				
				InterfaceConnectorPnlAdd connector = new ConnectorPnlAddCollaboratingEntity( me );
				PnlAdd myAddPanel = new PnlAdd( me, connector );
				
				me.setMainPanel( myAddPanel );
			}
		});
		mntmEntidadesColaboradoras.setFont(new Font("Dialog", Font.BOLD, 11)); //$NON-NLS-1$
		menuBar.add(mntmEntidadesColaboradoras);
		
		JMenuItem mntmSponsors = new JMenuItem(Messages.getString("PnlAppBody.mntmSponsors")); //$NON-NLS-1$
		mntmSponsors.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if ( !MyChangePanelManager.getChangePanelManager().setCurrentState( MyChangePanelManager.STATES.FREE ) ) return;
				
				InterfaceConnectorPnlAdd connector = new ConnectorPnlAddSponsor( me );
				PnlAdd myAddPanel = new PnlAdd( me, connector );
				
				me.setMainPanel( myAddPanel );
			}
		});
		mntmSponsors.setFont(new Font("Dialog", Font.BOLD, 11)); //$NON-NLS-1$
		menuBar.add(mntmSponsors);
		
		JMenuItem mntmClubsDeportivos = new JMenuItem(Messages.getString("PnlAppBody.mntmClubsDeportivos")); //$NON-NLS-1$
		mntmClubsDeportivos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if ( !MyChangePanelManager.getChangePanelManager().setCurrentState( MyChangePanelManager.STATES.FREE ) ) return;
				
				InterfaceConnectorPnlAdd connector = new ConnectorPnlAddClubs( me );
				PnlAdd myAddPanel = new PnlAdd( me, connector );
				
				me.setMainPanel( myAddPanel );
			}
		});
		mntmClubsDeportivos.setFont(new Font("Dialog", Font.BOLD, 11)); //$NON-NLS-1$
		menuBar.add(mntmClubsDeportivos);
		
		panelBody = new JPanel();
		panelBody.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		GridBagConstraints gbc_panelBody = new GridBagConstraints();
		gbc_panelBody.weighty = 0.5;
		gbc_panelBody.weightx = 0.5;
		gbc_panelBody.insets = new Insets(0, 0, 5, 5);
		gbc_panelBody.fill = GridBagConstraints.BOTH;
		gbc_panelBody.gridx = 1;
		gbc_panelBody.gridy = 4;
		add(panelBody, gbc_panelBody);
		panelBody.setLayout(new GridLayout(0, 1, 0, 0));

		//PnlAdd myAddPanel = new PnlAdd( this );
		ConnectorPnlAddCompetitor connector = new ConnectorPnlAddCompetitor( this.me );
		PnlAdd myAddPanel = new PnlAdd( this.me, connector );
		
		panelBody.add( myAddPanel );
	}
	
	public void setMainPanel( JPanel panel ) {
		this.panelBody.removeAll();
		this.panelBody.add( panel );
		this.panelBody.revalidate();
	}
}
