package presentacion;

import java.awt.Component;

import javax.swing.JOptionPane;

public class MyChangePanelManager {
	static public enum STATES {
		FREE, LOCK_SAVE
	}
	
	static private MyChangePanelManager mInstancia;
	
	private STATES currentState;

	private MyChangePanelManager() {
		this.currentState = STATES.FREE;
	}
	
	public static MyChangePanelManager getChangePanelManager() {
		try {
			if (mInstancia == null) {
				mInstancia = new MyChangePanelManager();
			}

			return mInstancia;
		} catch (Exception e) {
		}

		return null;
	}

	public STATES getCurrentState() {
		return currentState;
	}

	public boolean setCurrentState(STATES currentState) {
		switch ( this.currentState ) {
		case FREE:
			break;
			
		case LOCK_SAVE:
			if ( !MyMessageManager.getMessageManager().showConfirmDontSave() ) return false;
			break;

		default:
			break;
		}
		
		this.currentState = currentState;
		return true;
	}
}