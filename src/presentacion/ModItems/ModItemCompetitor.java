package presentacion.ModItems;

import java.awt.GridLayout;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import presentacion.ItemPanelResource;
import presentacion.AddPanels.InfoCompeticion;
import presentacion.AddPanels.InfoParticipante;
import presentacion.AddPanels.PnlSelectItems;
import presentacion.Messages.Messages;
import dominio.Competitor;
import dominio.Image;

public class ModItemCompetitor implements InterfaceModItem {
	private Competitor competitor;
	
	public ModItemCompetitor(Competitor competitor) {
		this.competitor = competitor;
	}
	
	public boolean save() {
		if ( !this.competitor.update() ) {
			return this.competitor.insert();
		}
		
		return true;
	}
	
	public Vector<ItemPanelResource> getItems() {
		Vector<ItemPanelResource> result = new Vector<ItemPanelResource>();
		ItemPanelResource resource;
		
		resource = new ItemPanelResource();
		resource.newMenu( Messages.getString("ModItemCompetitor.ItemInformacion") ); //$NON-NLS-1$
		
		JScrollPane jsp = new JScrollPane();
		jsp.setBorder(null);
		jsp.setViewportView( new InfoParticipante( this.competitor ) );
		
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(0, 1, 0, 0));
		p.add( jsp );
		resource.setPanel( p );
		result.add( resource );
		
		return result;
	}
	
	public String getText() {
		return this.competitor.toString();
	}
	
	public Image getImage() {
		return this.competitor.getPhoto();
	}
}
