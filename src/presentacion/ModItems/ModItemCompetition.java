package presentacion.ModItems;

import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import presentacion.ItemPanelResource;
import presentacion.AddPanels.InfoCompeticion;
import presentacion.AddPanels.InfoParticipante;
import presentacion.AddPanels.PnlSelectItems;
import presentacion.Editor.GraphicEditor;
import presentacion.Messages.Messages;
import presentacion.SelectedItems.InterfaceSelectedItems;
import presentacion.SelectedItems.SelectedItemsCollaboratingEntitiesInCompetition;
import presentacion.SelectedItems.SelectedItemsCompetitorsInCompetition;
import dominio.Competition;
import dominio.Competitor;
import dominio.Image;

public class ModItemCompetition implements InterfaceModItem {
	private Competition competition;
	
	public ModItemCompetition(Competition competition) {
		this.competition = competition;
	}
	
	public boolean save() {
		if ( !this.competition.update() ) {
			return this.competition.insert();
		}
		
		return true;
	}
	
	public Vector<ItemPanelResource> getItems() {
		Vector<ItemPanelResource> result = new Vector<ItemPanelResource>();
		ItemPanelResource resource;
		
		resource = new ItemPanelResource();
		resource.newMenu( Messages.getString("ModItemCompetition.ItemInformacion") ); //$NON-NLS-1$
		
		JScrollPane jsp = new JScrollPane();
		jsp.setBorder(null);
		jsp.setViewportView( new InfoCompeticion( this.competition ) );
		
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(0, 1, 0, 0));
		p.add( jsp );
		resource.setPanel( p );
		result.add( resource );
		
		resource = new ItemPanelResource();
		resource.newMenu( Messages.getString("ModItemCompetition.ItemParticipantes") ); //$NON-NLS-1$
		resource.setPanel( new PnlSelectItems( new SelectedItemsCompetitorsInCompetition( this.competition ) ) );
		result.add( resource );
		
		resource = new ItemPanelResource();
		resource.newMenu( Messages.getString("ModItemCompetition.ItemEntidadesColaboradoras") ); //$NON-NLS-1$
		resource.setPanel( new PnlSelectItems( new SelectedItemsCollaboratingEntitiesInCompetition( this.competition ) ) );
		result.add( resource );
		
		resource = new ItemPanelResource();
		resource.newMenu( Messages.getString("ModItemCompetition.ItemMapa") ); //$NON-NLS-1$
		resource.setPanel( new GraphicEditor( this.competition ) );
		result.add( resource );
		
		return result;
	}

	public String getText() {
		return this.competition.toString();
	}
	
	public Image getImage() {
		return this.competition.getPoster();
	}
}
