package presentacion.ModItems;

import java.util.Vector;

import dominio.Image;
import presentacion.ItemPanelResource;

public interface InterfaceModItem {
	public boolean save();
	public Image getImage();
	public String getText();
	public Vector<ItemPanelResource> getItems();
}
